package com.tockacrta.trailOer.reports.model;

import com.tccrt.chemistryPdf.model.Atom;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetitor;

public class TorCmrCmnRunAtom extends Atom<TorCmrCmnRun> {

	public TorCmrCmnRunAtom(TorCmrCmnRun value) {
		super(value);
		setFormat("%s, %s %s");
	}

	@Override
	protected Object formatOutput(String format, TorCmrCmnRun value) {
		TorCompetitor competitor=getValue().getTorCmrCmn().getTorCompetitor();
		// first and last are required
		String midName="";
		if (competitor.getMidName()!=null) {
			midName=competitor.getMidName();
		}
		return String.format(format, competitor.getLastName(), competitor.getFirstName(), midName);
	}
	
	
}
