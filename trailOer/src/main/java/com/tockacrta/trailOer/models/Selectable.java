package com.tockacrta.trailOer.models;

public class Selectable<T> {
	private T data;
	private Boolean selected;
	
	public Selectable(T data, Boolean selected) {
		super();
		this.data = data;
		this.selected = selected;
	}
	
	public T getData() {
		return data;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setData(T data) {
		this.data = data;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	
}
