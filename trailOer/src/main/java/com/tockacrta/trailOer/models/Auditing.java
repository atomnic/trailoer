package com.tockacrta.trailOer.models;

import java.util.Date;

public interface Auditing {
	void setCreatedBy(String createdBy);
	void setDateCreated(Date dateCreated);
	
	String getCreatedBy();
	Date getDateCreated();
}
