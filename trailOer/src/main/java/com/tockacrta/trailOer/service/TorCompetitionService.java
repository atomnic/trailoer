package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;

public interface TorCompetitionService extends DomainService<TorCompetition, Long>  {
	
	@Override
	public TorCompetition save(TorCompetition entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCompetition> entities) throws ServiceException;
	
	
	public List<TorCompetition> getByExample(TorCompetition example);

	public List<TorCompetition> getByExample(TorCompetition example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorCompetition example);	
	
	public List<TorCompetition> getByEvent(TorEvent evn);
	public List<TorCompetition> getByCompetitor(TorCompetitor cmr);
}