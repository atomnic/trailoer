package com.tockacrta.trailOer.service.ranking;

import java.util.List;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.service.ranking.model.PreOData;

public interface PreORankingService {
	PreOData preliminaryResults(TorCmrCmnRun torCmrCmnRun);
	List<PreOData> preOPreliminary(TorEvent torEvent, TorCompetition torCompetition, TorRun torRun, TorCategory torCategory);
	List<PreOData> preOFinal();
}
