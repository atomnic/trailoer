package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.service.TorCompetitionService;

public class TorCompetitionServiceImpl extends BaseDomainServiceImpl<TorCompetition, Long> implements TorCompetitionService {

	public TorCompetitionServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCompetitionDAO();
	}
	
	public List<TorCompetition> getByExample(TorCompetition example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCompetition example) {
		return daoFactory.getTorCompetitionDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCompetition> getByExample(TorCompetition example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCompetitionDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorCompetition save(TorCompetition entity) throws ServiceException {
		TorCompetition ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCompetition> entities) throws ServiceException {
		for (TorCompetition entity : entities) {
			save(entity);
		}
	}

	@Override
	public List<TorCompetition> getByEvent(TorEvent evn) {
		return daoFactory.getTorCompetitionDAO().findByEvent(evn);
	}

	@Override
	public List<TorCompetition> getByCompetitor(TorCompetitor cmr) {
		return daoFactory.getTorCompetitionDAO().findByCompetitor(cmr);
	}

	
}