package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.service.TorEventService;

public class TorEventServiceImpl extends BaseDomainServiceImpl<TorEvent, Long> implements TorEventService {

	public TorEventServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorEventDAO();
	}
	
	public List<TorEvent> getByExample(TorEvent example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorEvent example) {
		return daoFactory.getTorEventDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorEvent> getByExample(TorEvent example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorEventDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorEvent save(TorEvent TorEvent) throws ServiceException {
		TorEvent ret= super.save(TorEvent);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorEvent> entities) throws ServiceException {
		for (TorEvent TorEvent : entities) {
			save(TorEvent);
		}
	}

	@Override
	public TorEvent getOpen() {
		List<TorEvent> open=daoFactory.getTorEventDAO().getOpen();
		if (open.size()>1) {
			// TODO: report error, or implement additional logic
			return null;
		}
		if (open.size()==1) {
			return open.get(0);
		}
		return null;
		
	}


}