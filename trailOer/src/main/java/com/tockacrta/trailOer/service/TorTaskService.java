package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorTask;

public interface TorTaskService extends DomainService<TorTask, Long>  {
	
	@Override
	public TorTask save(TorTask entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorTask> entities) throws ServiceException;
	
	
	public List<TorTask> getByExample(TorTask example);

	public List<TorTask> getByExample(TorTask example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorTask example);

}