package com.tockacrta.trailOer.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.tockacrta.trailOer.dao.BaseDAO;
import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.service.DomainService;

public abstract class BaseDomainServiceImpl<T, ID extends Serializable> 
	// extends BaseServiceImpl 
	implements DomainService<T, ID> {
	
	protected DaoFactory daoFactory;
	
	public DaoFactory getDaoFactory() {
	//log.info("getting daoFactory");
	return daoFactory;
	}
	
	
	public BaseDomainServiceImpl() {
	super();
	// TODO Auto-generated constructor stub
	}
	
	protected BaseDAO<T, ID> defaultDAO;
	
	protected abstract void setDefaultDAO();
	
	public void setDaoFactory(DaoFactory daoFactory) {
	//super.setDaoFactory(daoFactory);
	// log.info("daoFactory set");
	this.daoFactory = daoFactory;
	setDefaultDAO();
	}
	
	public void delete(T entity) {
	defaultDAO.delete(entity);
	}
	
	public T get(ID id) {
	return defaultDAO.findById(id);
	}
	
	public List<T> getAll() {
	return defaultDAO.findAll();
	}
	
	
	public T save(T entity) {
	return defaultDAO.save(entity);
	}
	
	public void saveAll(Collection<T> entities) {
	defaultDAO.saveAll(entities);
	}

}