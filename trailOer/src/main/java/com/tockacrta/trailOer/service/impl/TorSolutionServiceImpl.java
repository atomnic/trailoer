package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorSolution;
import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.service.TorSolutionService;

public class TorSolutionServiceImpl extends BaseDomainServiceImpl<TorSolution, Long> implements TorSolutionService {

	public TorSolutionServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorSolutionDAO();
	}
	
	public List<TorSolution> getByExample(TorSolution example, boolean solutionEntry) {
		return getByExample(example, solutionEntry, 0, 0, null);
	}

	public int getByExampleSize(TorSolution example, boolean solutionEntry) {
		return daoFactory.getTorSolutionDAO().findByExampleSize(example,solutionEntry);
	}
	
	@Override
	public List<TorSolution> getByExample(TorSolution example,boolean solutionEntry, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorSolutionDAO().findByExample(example,solutionEntry, firstElement, maxElements, sort);
	}
	

	@Override
	public TorSolution save(TorSolution entity) throws ServiceException {
		TorSolution ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorSolution> entities) throws ServiceException {
		for (TorSolution entity : entities) {
			save(entity);
		}
	}

	@Override
	public void generate(TorCmrCmnRun torCmrCmnRun) {
		if (torCmrCmnRun!=null) {
			List<TorTask> torTasks= daoFactory.getTorTaskDAO().findByCriteria(Restrictions.eq("torRun", torCmrCmnRun.getTorRun()), 
												Restrictions.eq("torCategory", torCmrCmnRun.getTorCmrCmn().getTorCategory()));
			for (TorTask torTask : torTasks) {
				TorSolution torSolution=new TorSolution();
				torSolution.setTorCmrCmnRun(torCmrCmnRun);
				torSolution.setTorTask(torTask);
				save(torSolution);
			}
		}
	}
	
	@Override
	public void generateAll(List<TorCmrCmnRun> torCmrCmnRuns) {
		for (TorCmrCmnRun torCmrCmnRun : torCmrCmnRuns) {
			generate(torCmrCmnRun);
		}
		
	}


}