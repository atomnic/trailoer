package com.tockacrta.trailOer.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface DomainService<T, ID extends Serializable> extends BaseService{
	public T save(T entity);
	public void saveAll(Collection<T> entities);
	public void delete(T entity);
	public T get(ID id);
	public List<T> getAll();

}
