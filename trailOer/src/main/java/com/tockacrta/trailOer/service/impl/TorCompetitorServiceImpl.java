package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorTypes;
import com.tockacrta.trailOer.service.TorCompetitorService;
import com.tockacrta.trailOer.service.TorCountryService;

public class TorCompetitorServiceImpl extends BaseDomainServiceImpl<TorCompetitor, Long> implements TorCompetitorService {

	public TorCompetitorServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCompetitorDAO();
	}
	
	public List<TorCompetitor> getByExample(TorCompetitor example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCompetitor example) {
		return daoFactory.getTorCompetitorDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCompetitor> getByExample(TorCompetitor example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCompetitorDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorCompetitor save(TorCompetitor torCompetitor) throws ServiceException {
		TorCompetitor ret= super.save(torCompetitor);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCompetitor> entities) throws ServiceException {
		for (TorCompetitor torCountry : entities) {
			save(torCountry);
		}
	}

	@Override
	public List<TorCompetitor> getByType(String typeCode, Integer firstElement,
			Integer maxElements, Map<String, Boolean> sorts) {
		TorTypes torType=daoFactory.getTorTypesDAO().findUniqueByCriteria(Restrictions.eq("code", typeCode));
		TorCompetitor example=new TorCompetitor();
		example.setTorType(torType);
		return daoFactory.getTorCompetitorDAO().findByExample(example, firstElement, maxElements, sorts);
	}

	@Override
	public int getByTypeSize(String typeCode) {
		TorTypes torType=daoFactory.getTorTypesDAO().findUniqueByCriteria(Restrictions.eq("code", typeCode));
		TorCompetitor example=new TorCompetitor();
		example.setTorType(torType);
		return daoFactory.getTorCompetitorDAO().findByExampleSize(example);
	}


}