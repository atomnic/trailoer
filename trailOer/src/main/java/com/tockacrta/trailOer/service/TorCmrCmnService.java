package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.models.Selectable;

public interface TorCmrCmnService extends DomainService<TorCmrCmn, Long>  {
	
	@Override
	public TorCmrCmn save(TorCmrCmn entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCmrCmn> entities) throws ServiceException;
	
	
	public List<TorCmrCmn> getByExample(TorCmrCmn example);

	public List<TorCmrCmn> getByExample(TorCmrCmn example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorCmrCmn example);		
	
	public List<Selectable<TorCmrCmn>> getByCmrAndEvn(TorCompetitor cmr, TorEvent evn, Boolean suggested);
	public void saveAll(List<Selectable<TorCmrCmn>> selectables);
	
	public void setTorCmrCmnRunService(TorCmrCmnRunService torCmrCmnRunService);
	
}