package com.tockacrta.trailOer.service.ranking.model;

import java.util.Comparator;

public class PreODataComparator implements Comparator<PreOData> {

	@Override
	public int compare(PreOData o1, PreOData o2) {
		return o1.getPointsAll().compareTo(o2.getPointsAll());
	}

}
