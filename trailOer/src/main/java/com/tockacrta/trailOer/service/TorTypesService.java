package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorTypes;

public interface TorTypesService extends DomainService<TorTypes, Long>  {
	
	@Override
	public TorTypes save(TorTypes entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorTypes> entities) throws ServiceException;
	
	
	public List<TorTypes> getByExample(TorTypes example);

	public List<TorTypes> getByExample(TorTypes example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorTypes example);	
	
	public TorTypes getByDomain(String domain, String code);
}