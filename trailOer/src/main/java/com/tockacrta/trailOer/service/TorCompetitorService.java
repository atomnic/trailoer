package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCompetitor;

public interface TorCompetitorService extends DomainService<TorCompetitor, Long>  {
	
	@Override
	public TorCompetitor save(TorCompetitor entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCompetitor> entities) throws ServiceException;
	
	
	public List<TorCompetitor> getByExample(TorCompetitor example);

	public List<TorCompetitor> getByExample(TorCompetitor example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorCompetitor example);
	
	public List<TorCompetitor> getByType(String typeCode,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	public int getByTypeSize(String typeCode);	
}