package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorEvent;

public interface TorEventService extends DomainService<TorEvent, Long>  {
	
	@Override
	public TorEvent save(TorEvent entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorEvent> entities) throws ServiceException;
	
	
	public List<TorEvent> getByExample(TorEvent example);

	public List<TorEvent> getByExample(TorEvent example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorEvent example);	
	
	public TorEvent getOpen();
}