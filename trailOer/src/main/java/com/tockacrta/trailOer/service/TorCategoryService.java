package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCategory;

public interface TorCategoryService extends DomainService<TorCategory, Long>  {
	
	@Override
	public TorCategory save(TorCategory entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCategory> entities) throws ServiceException;
	
	
	public List<TorCategory> getByExample(TorCategory example);

	public List<TorCategory> getByExample(TorCategory example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorCategory example);	
}