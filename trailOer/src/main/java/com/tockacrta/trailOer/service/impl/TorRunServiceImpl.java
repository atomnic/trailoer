package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.service.TorRunService;

public class TorRunServiceImpl extends BaseDomainServiceImpl<TorRun, Long> implements TorRunService {

	public TorRunServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorRunDAO();
	}
	
	public List<TorRun> getByExample(TorRun example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorRun example) {
		return daoFactory.getTorRunDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorRun> getByExample(TorRun example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorRunDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorRun save(TorRun entity) throws ServiceException {
		TorRun ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorRun> entities) throws ServiceException {
		for (TorRun entity : entities) {
			save(entity);
		}
	}


}