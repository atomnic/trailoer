package com.tockacrta.trailOer.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tockacrta.trailOer.dao.DaoFactory;

public class BaseServiceImpl {
	private static final Log log = LogFactory.getLog(BaseServiceImpl.class);
	protected DaoFactory daoFactory;

	public DaoFactory getDaoFactory() {
		log.info("getting daoFactory");
		return daoFactory;
	}

	public void setDaoFactory(DaoFactory daoFactory) {
		log.info("daoFactory set");
		this.daoFactory = daoFactory;
	}
}
