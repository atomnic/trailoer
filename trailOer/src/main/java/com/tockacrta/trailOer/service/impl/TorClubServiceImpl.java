package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorClub;
import com.tockacrta.trailOer.service.TorClubService;

public class TorClubServiceImpl extends BaseDomainServiceImpl<TorClub, Long> implements TorClubService {

	public TorClubServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorClubDAO();
	}
	
	public List<TorClub> getByExample(TorClub example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorClub example) {
		return daoFactory.getTorClubDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorClub> getByExample(TorClub example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorClubDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorClub save(TorClub entity) throws ServiceException {
		TorClub ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorClub> entities) throws ServiceException {
		for (TorClub entity : entities) {
			save(entity);
		}
	}


}