package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCountry;
import com.tockacrta.trailOer.service.TorCountryService;

public class TorCountryServiceImpl extends BaseDomainServiceImpl<TorCountry, Long> implements TorCountryService {

	public TorCountryServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCountryDAO();
	}
	
	public List<TorCountry> getByExample(TorCountry example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCountry example) {
		return daoFactory.getTorCountryDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCountry> getByExample(TorCountry example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCountryDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorCountry save(TorCountry torCountry) throws ServiceException {
		TorCountry ret= super.save(torCountry);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCountry> entities) throws ServiceException {
		for (TorCountry torCountry : entities) {
			save(torCountry);
		}
	}


}