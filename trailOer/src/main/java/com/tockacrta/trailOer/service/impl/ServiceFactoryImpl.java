package com.tockacrta.trailOer.service.impl;

import com.tockacrta.trailOer.service.ServiceFactory;
import com.tockacrta.trailOer.service.TorCategoryService;
import com.tockacrta.trailOer.service.TorClubService;
import com.tockacrta.trailOer.service.TorCmrCmnRunService;
import com.tockacrta.trailOer.service.TorCmrCmnService;
import com.tockacrta.trailOer.service.TorCompetitionService;
import com.tockacrta.trailOer.service.TorCompetitorService;
import com.tockacrta.trailOer.service.TorComplainsService;
import com.tockacrta.trailOer.service.TorCountryService;
import com.tockacrta.trailOer.service.TorEventService;
import com.tockacrta.trailOer.service.TorRunService;
import com.tockacrta.trailOer.service.TorSolutionService;
import com.tockacrta.trailOer.service.TorTaskService;
import com.tockacrta.trailOer.service.TorTypesService;
import com.tockacrta.trailOer.service.ranking.PreORankingService;

public class ServiceFactoryImpl implements ServiceFactory {

	private TorCategoryService torCategoryService;
	private TorCmrCmnRunService torCmrCmnRunService;
	private TorCmrCmnService torCmrCmnService;
	private TorClubService torClubService;
	private TorCompetitionService torCompetitionService;
	private TorComplainsService torComplainsService;
	private TorRunService torRunService;
	private TorSolutionService torSolutionService;
	private TorTaskService torTaskService;
	private TorTypesService torTypesService;		
	private TorCountryService torCountryService;
	private TorCompetitorService torCompetitorService;
	private TorEventService torEventService;
	private PreORankingService preORankingService;
	
	public void setTorCountryService(TorCountryService torCountryService) {
		this.torCountryService=torCountryService;
	}
	
	@Override
	public TorCountryService getTorCountryService() {
		return this.torCountryService;
	}
	
	@Override
	public TorCompetitorService getTorCompetitorService() {
		return torCompetitorService;
	}

	public void setTorCompetitorService(TorCompetitorService torCompetitorService) {
		this.torCompetitorService = torCompetitorService;
	}

	public TorEventService getTorEventService() {
		return torEventService;
	}

	public void setTorEventService(TorEventService torEventService) {
		this.torEventService = torEventService;
	}

	public TorCategoryService getTorCategoryService() {
		return torCategoryService;
	}

	public void setTorCategoryService(TorCategoryService torCategoryService) {
		this.torCategoryService = torCategoryService;
	}

	public TorCmrCmnRunService getTorCmrCmnRunService() {
		return torCmrCmnRunService;
	}

	public void setTorCmrCmnRunService(TorCmrCmnRunService torCmrCmnRunService) {
		this.torCmrCmnRunService = torCmrCmnRunService;
	}

	public TorCmrCmnService getTorCmrCmnService() {
		return torCmrCmnService;
	}

	public void setTorCmrCmnService(TorCmrCmnService torCmrCmnService) {
		this.torCmrCmnService = torCmrCmnService;
	}

	public TorClubService getTorClubService() {
		return torClubService;
	}

	public void setTorClubService(TorClubService torClubService) {
		this.torClubService = torClubService;
	}

	public TorCompetitionService getTorCompetitionService() {
		return torCompetitionService;
	}

	public void setTorCompetitionService(TorCompetitionService torCompetitionService) {
		this.torCompetitionService = torCompetitionService;
	}

	public TorComplainsService getTorComplainsService() {
		return torComplainsService;
	}

	public void setTorComplainsService(TorComplainsService torComplainsService) {
		this.torComplainsService = torComplainsService;
	}

	public TorRunService getTorRunService() {
		return torRunService;
	}

	public void setTorRunService(TorRunService torRunService) {
		this.torRunService = torRunService;
	}

	public TorSolutionService getTorSolutionService() {
		return torSolutionService;
	}

	public void setTorSolutionService(TorSolutionService torSolutionService) {
		this.torSolutionService = torSolutionService;
	}

	public TorTaskService getTorTaskService() {
		return torTaskService;
	}

	public void setTorTaskService(TorTaskService torTaskService) {
		this.torTaskService = torTaskService;
	}

	public TorTypesService getTorTypesService() {
		return torTypesService;
	}

	public void setTorTypesService(TorTypesService torTypesService) {
		this.torTypesService = torTypesService;
	}
	

	public void setPreORankingService(PreORankingService preORankingService) {
		this.preORankingService=preORankingService;
	}

	@Override
	public PreORankingService getPreORankingService() {
		return this.preORankingService;
	}

}
