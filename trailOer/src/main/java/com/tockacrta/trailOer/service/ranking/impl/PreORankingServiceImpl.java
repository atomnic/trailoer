package com.tockacrta.trailOer.service.ranking.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorClub;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorSolution;
import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.service.DomainService;
import com.tockacrta.trailOer.service.TorCmrCmnRunService;
import com.tockacrta.trailOer.service.TorSolutionService;
import com.tockacrta.trailOer.service.TorTaskService;
import com.tockacrta.trailOer.service.impl.BaseDomainServiceImpl;
import com.tockacrta.trailOer.service.ranking.PreORankingService;
import com.tockacrta.trailOer.service.ranking.model.PreOData;
import com.tockacrta.trailOer.service.ranking.model.PreODataComparator;

public class PreORankingServiceImpl extends BaseDomainServiceImpl<TorCmrCmnRun, Long>  implements PreORankingService {

	private TorCmrCmnRunService torCmrCmnRunService;
	private TorTaskService  torTaskService;
	private static final int PENALTY_TEMPO = 30;
	private static final int PENALTY_PREO = 60;
	
	@Override
	public PreOData preliminaryResults(TorCmrCmnRun torCmrCmnRun0) {
		TorCmrCmnRun torCmrCmnRun=daoFactory.getTorCmrCmnRunDAO().findById(torCmrCmnRun0.getId());
		PreOData preOData=new PreOData();
		if (torCmrCmnRun.getFinishTime()==null) {
			return preOData;
		}		
		int penaltyTime=PENALTY_TEMPO;
		String type="T";
		if (torCmrCmnRun.getTorRun().getTorTypes().getCode().equals("P")) {
			penaltyTime=PENALTY_PREO;
			type="P";
		}
		Integer points=0;
		BigDecimal time=new BigDecimal(0);
		for (Object object : torCmrCmnRun.getTorSolutions()) {
			TorSolution torSolution =(TorSolution) object;
			TorTask torTask=torTaskService.get(torSolution.getTorTask().getId());
			if (torTask.getTorTypeType().getCode().equals("ct")
					&& torTask.getTorTypeStatus().getCode().equals("regular")
					&& torSolution.getSolution().equals(torTask.getSolution()) ) {
				points++;
			}
			else if ( torTask.getTorTypeType().getCode().equals("st")
					&& torTask.getTorTypeStatus().getCode().equals("regular")) {
				   if (torSolution.getTime()==null) {
					   time=time.add(new BigDecimal(120*1000));
				   } else {
					   time=time.add(torSolution.getTime());
				   }
			}					
			else if ( torTask.getTorTypeType().getCode().equals("tc")
					&& torTask.getTorTypeStatus().getCode().equals("regular")) {
					if (torSolution.getSolution()==null || !torSolution.getSolution().equals(torTask.getSolution())) {								
						time=time.add(new BigDecimal(penaltyTime));  // TODO hardcoded penalty for PreO TC
					}
			}
		}
		preOData.setPenaltyPoints(0);
		float diffMin=0;
		if (type.equals("P")) {
			long diff=torCmrCmnRun.getFinishTime().getTime()-torCmrCmnRun.getActualStart().getTime();
			diffMin= (diff/60000.0F);
			long timeLImit=torCmrCmnRun.getTorRun().getTimeLimit();
			if (torCmrCmnRun.getTorCmrCmn().getTorCategory().getCode().equals("A")) {  // TODO RIJEŠITI!!!
				timeLImit=65;
			}			
			if ( diffMin>timeLImit) {
				float p=(diffMin-timeLImit)/5F;
				double penaltyD=Math.ceil((double)p);
				Integer penalty=(int)penaltyD;
				preOData.setPenaltyPoints(penalty);
			}
		}
		preOData.setTime((int)diffMin);
		preOData.setPoints(points);
		preOData.setPointsAll(points-preOData.getPenaltyPoints());
		preOData.setTorCmrCmnRun(torCmrCmnRun);
		preOData.setTcTime(time);
		return preOData;
	}

	@Override
	public List<PreOData> preOPreliminary(TorEvent torEvent, TorCompetition torCompetition, TorRun torRun, TorCategory torCategory) {
		List<PreOData> results=new ArrayList<PreOData>();
		int penaltyTime=PENALTY_TEMPO;
		if (torRun!=null) {			
			if (torRun.getTorTypes().getCode().equals("P")) {
				penaltyTime=PENALTY_PREO;
			}
			TorCmrCmnRun torCmrCmnRunExample=new TorCmrCmnRun();
			torCmrCmnRunExample.setTorRun(torRun);
			List<TorCmrCmnRun>  torCmrCmnRuns=torCmrCmnRunService.getByExampleCategory(torCmrCmnRunExample, torCategory, 0, 0, null);
			for (TorCmrCmnRun torCmrCmnRun : torCmrCmnRuns) {
				if (torCmrCmnRun.getFinishTime()==null) {
					continue;
				}
				PreOData preOData=new PreOData();
				Integer points=0;
				BigDecimal time=new BigDecimal(0);
				for (Object object : torCmrCmnRun.getTorSolutions()) {
					TorSolution torSolution =(TorSolution) object;
					TorTask torTask=torTaskService.get(torSolution.getTorTask().getId());
					if (torTask.getTorTypeType().getCode().equals("ct")
							&& torTask.getTorTypeStatus().getCode().equals("regular")
							&& torSolution.getSolution().equals(torTask.getSolution()) ) {
						points++;
					}
					else if ( torTask.getTorTypeType().getCode().equals("st")
							&& torTask.getTorTypeStatus().getCode().equals("regular")) {
						   if (torSolution.getTime()==null) {
							   time=time.add(new BigDecimal(120*1000));
						   } else {
							   time=time.add(torSolution.getTime());
						   }
					}					
					else if ( torTask.getTorTypeType().getCode().equals("tc")
							&& torTask.getTorTypeStatus().getCode().equals("regular")) {
							if (torSolution.getSolution()==null || !torSolution.getSolution().equals(torTask.getSolution())) {								
								time=time.add(new BigDecimal(penaltyTime));  // TODO hardcoded penalty for PreO TC
							}
					}
				}
				preOData.setPenaltyPoints(0);
				float diffMin=0;
				if (torRun.getTorTypes().getCode().equals("P")) {
					long diff=torCmrCmnRun.getFinishTime().getTime()-torCmrCmnRun.getActualStart().getTime();
					diffMin= (diff/60000.0F);
					long timeLImit=torCmrCmnRun.getTorRun().getTimeLimit();
					if (torCmrCmnRun.getTorCmrCmn().getTorCategory().getCode().equals("A")) {  // TODO RIJEŠITI!!!
						timeLImit=65;
					}			
					if ( diffMin>timeLImit) {
						float p=(diffMin-timeLImit)/5F;
						double penaltyD=Math.ceil((double)p);
						Integer penalty=(int)penaltyD;
						preOData.setPenaltyPoints(penalty);
					}
				}
				preOData.setTime((int)diffMin);
				preOData.setPoints(points);
				preOData.setPointsAll(points-preOData.getPenaltyPoints());
				preOData.setTorCmrCmnRun(torCmrCmnRun);
				preOData.setTcTime(time);
				results.add(preOData);
			}
			if (torRun.getTorTypes().getCode().equals("P")) {
				Collections.sort(results, new Comparator<PreOData>() {
	
					@Override
					public int compare(PreOData o1, PreOData o2) {
						if (o1.getPointsAll().equals(o2.getPointsAll())) {
							return o1.getTcTime().compareTo(o2.getTcTime());
						}
						return -1*(o1.getPointsAll().compareTo(o2.getPointsAll()));
					}
					
				});	
			} else if (torRun.getTorTypes().getCode().equals("T")) {
				Collections.sort(results, new Comparator<PreOData>() {
	
					@Override
					public int compare(PreOData o1, PreOData o2) {
						return o1.getTcTime().compareTo(o2.getTcTime());
					}
					
				});					
			}

		}
		return results;
	}

	@Override
	public List<PreOData> preOFinal() {
		// TODO Auto-generated method stub
		return null;
	}

	public TorCmrCmnRunService getTorCmrCmnRunService() {
		return torCmrCmnRunService;
	}

	public void setTorCmrCmnRunService(TorCmrCmnRunService torCmrCmnRunService) {
		this.torCmrCmnRunService = torCmrCmnRunService;
	}

	@Override
	protected void setDefaultDAO() {
		// TODO Auto-generated method stub
		
	}

	public TorTaskService getTorTaskService() {
		return torTaskService;
	}

	public void setTorTaskService(TorTaskService torTaskService) {
		this.torTaskService = torTaskService;
	}



}
