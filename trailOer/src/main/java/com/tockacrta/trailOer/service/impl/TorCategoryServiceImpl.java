package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.service.TorCategoryService;

public class TorCategoryServiceImpl extends BaseDomainServiceImpl<TorCategory, Long> implements TorCategoryService {

	public TorCategoryServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCategoryDAO();
	}
	
	public List<TorCategory> getByExample(TorCategory example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCategory example) {
		return daoFactory.getTorCategoryDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCategory> getByExample(TorCategory example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCategoryDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorCategory save(TorCategory entity) throws ServiceException {
		TorCategory ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCategory> entities) throws ServiceException {
		for (TorCategory entity : entities) {
			save(entity);
		}
	}


}