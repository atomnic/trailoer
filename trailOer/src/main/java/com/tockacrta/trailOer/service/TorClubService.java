package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorClub;

public interface TorClubService extends DomainService<TorClub, Long>  {
	
	@Override
	public TorClub save(TorClub entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorClub> entities) throws ServiceException;
	
	
	public List<TorClub> getByExample(TorClub example);

	public List<TorClub> getByExample(TorClub example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorClub example);	
}