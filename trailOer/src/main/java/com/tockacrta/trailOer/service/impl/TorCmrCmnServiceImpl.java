package com.tockacrta.trailOer.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.models.Selectable;
import com.tockacrta.trailOer.service.TorCmrCmnRunService;
import com.tockacrta.trailOer.service.TorCmrCmnService;

public class TorCmrCmnServiceImpl extends BaseDomainServiceImpl<TorCmrCmn, Long> implements TorCmrCmnService {

	private TorCmrCmnRunService torCmrCmnRunService;
	
	public TorCmrCmnServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCmrCmnDAO();
	}
	
	public List<TorCmrCmn> getByExample(TorCmrCmn example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCmrCmn example) {
		return daoFactory.getTorCmrCmnDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCmrCmn> getByExample(TorCmrCmn example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCmrCmnDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorCmrCmn save(TorCmrCmn entity) throws ServiceException {
		TorCmrCmn ret= super.save(entity);
		torCmrCmnRunService.generate(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCmrCmn> entities) throws ServiceException {
		for (TorCmrCmn entity : entities) {
			save(entity);
		}
	}

	@Override
	public List<Selectable<TorCmrCmn>> getByCmrAndEvn(TorCompetitor cmr, TorEvent evn, Boolean suggested) {
		List<TorCmrCmn> selected= daoFactory.getTorCmrCmnDAO().findByCmrAndEvn(cmr, evn);		
		
		List<Selectable<TorCmrCmn>> result=new ArrayList<Selectable<TorCmrCmn>>();
		for (TorCmrCmn torCmrCmn : selected) {
			result.add(new Selectable<TorCmrCmn>(torCmrCmn, true));
		}
		
		if (suggested) {
			List<TorCompetition> unselected = daoFactory.getTorCompetitionDAO().findByEvnAndCmr(evn, cmr, false); 

			for (TorCompetition torCompetition : unselected) {
				TorCmrCmn newCmm=new TorCmrCmn();
				newCmm.setTorCompetition(torCompetition);
				newCmm.setTorCompetitor(cmr);
				result.add(new Selectable<TorCmrCmn>(newCmm, false));
			}
		}
		
		return result;
	}

	@Override
	public void delete(TorCmrCmn entity) {
		for (TorCmrCmnRun torCmrCmnRun : daoFactory.getTorCmrCmnRunDAO().findByCriteria(Restrictions.eq("torCmrCmn", entity))) {
			daoFactory.getTorCmrCmnRunDAO().delete(torCmrCmnRun);
		}
		super.delete(entity);
	};
	
	@Override
	public void saveAll(List<Selectable<TorCmrCmn>> selectables) {
		for (Selectable<TorCmrCmn> selectable : selectables) {
			if (selectable.getSelected()) {
				save(selectable.getData());
			} else if (selectable.getData().getId()!=null) {
				delete(selectable.getData());
			}
		}
	}

	public TorCmrCmnRunService getTorCmrCmnRunService() {
		return torCmrCmnRunService;
	}

	public void setTorCmrCmnRunService(TorCmrCmnRunService torCmrCmnRunService) {
		this.torCmrCmnRunService = torCmrCmnRunService;
	}


}