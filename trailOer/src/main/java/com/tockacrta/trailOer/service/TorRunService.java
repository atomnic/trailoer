package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorRun;

public interface TorRunService extends DomainService<TorRun, Long>  {
	
	@Override
	public TorRun save(TorRun entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorRun> entities) throws ServiceException;
	
	
	public List<TorRun> getByExample(TorRun example);

	public List<TorRun> getByExample(TorRun example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorRun example);	
}