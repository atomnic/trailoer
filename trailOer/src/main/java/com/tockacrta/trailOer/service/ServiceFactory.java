package com.tockacrta.trailOer.service;

import com.tockacrta.trailOer.service.ranking.PreORankingService;

public interface ServiceFactory {
	public TorCategoryService getTorCategoryService();
	public TorCmrCmnRunService getTorCmrCmnRunService();
	public TorCmrCmnService getTorCmrCmnService();
	public TorClubService getTorClubService();
	public TorCompetitionService getTorCompetitionService();
	public TorComplainsService getTorComplainsService();
	public TorRunService getTorRunService();
	public TorSolutionService getTorSolutionService();
	public TorTaskService getTorTaskService();
	public TorTypesService getTorTypesService();		
	public TorCountryService getTorCountryService();
	public TorCompetitorService getTorCompetitorService();
	public TorEventService getTorEventService();
	public PreORankingService getPreORankingService();
}