package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorComplains;
import com.tockacrta.trailOer.service.TorComplainsService;

public class TorComplainsServiceImpl extends BaseDomainServiceImpl<TorComplains, Long> implements TorComplainsService {

	public TorComplainsServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorComplainsDAO();
	}
	
	public List<TorComplains> getByExample(TorComplains example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorComplains example) {
		return daoFactory.getTorComplainsDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorComplains> getByExample(TorComplains example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorComplainsDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorComplains save(TorComplains entity) throws ServiceException {
		TorComplains ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorComplains> entities) throws ServiceException {
		for (TorComplains entity : entities) {
			save(entity);
		}
	}


}