package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCountry;

public interface TorCountryService extends DomainService<TorCountry, Long>  {
	
	@Override
	public TorCountry save(TorCountry entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCountry> entities) throws ServiceException;
	
	
	public List<TorCountry> getByExample(TorCountry example);

	public List<TorCountry> getByExample(TorCountry example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorCountry example);	
}