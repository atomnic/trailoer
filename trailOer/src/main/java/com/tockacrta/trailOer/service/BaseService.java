package com.tockacrta.trailOer.service;

import com.tockacrta.trailOer.dao.DaoFactory;

public interface BaseService {
	public void setDaoFactory(DaoFactory daoFactory);
}
