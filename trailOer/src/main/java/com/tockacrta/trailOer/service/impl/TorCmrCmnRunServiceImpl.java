package com.tockacrta.trailOer.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCountry;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorTypes;
import com.tockacrta.trailOer.service.TorCmrCmnRunService;

public class TorCmrCmnRunServiceImpl extends BaseDomainServiceImpl<TorCmrCmnRun, Long> implements TorCmrCmnRunService {

	private TorTypes torTypeStatus;
	private TorTypes ccrStatus;
	
	public TorCmrCmnRunServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorCmrCmnRunDAO();
	}
	
	public List<TorCmrCmnRun> getByExample(TorCmrCmnRun example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorCmrCmnRun example) {
		return daoFactory.getTorCmrCmnRunDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorCmrCmnRun> getByExample(TorCmrCmnRun example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorCmrCmnRunDAO().findByExample(example, firstElement, maxElements, sort);
	}
	
	public List<TorCmrCmnRun> getByExampleCategory(TorCmrCmnRun example, TorCategory torCategory, Integer firstElement, Integer maxElements, Map<String, Boolean> sorts) {
		return daoFactory.getTorCmrCmnRunDAO().findByExampleCategory(example, torCategory, firstElement, maxElements, sorts);
	}

	@Override
	public TorCmrCmnRun save(TorCmrCmnRun entity) throws ServiceException {
		TorCmrCmnRun ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorCmrCmnRun> entities) throws ServiceException {
		for (TorCmrCmnRun entity : entities) {
			save(entity);
		}
	}

	@Override
	public void generate(TorCmrCmn torCmrCmn) {
		// TODO cacheing needed
		if (torTypeStatus==null) {
			torTypeStatus=daoFactory.getTorTypesDAO().findUniqueByCriteria(Restrictions.eq("domain", "status"), Restrictions.eq("code", "open"));
		}
		if (ccrStatus==null) {
			ccrStatus=daoFactory.getTorTypesDAO().findUniqueByCriteria(Restrictions.eq("domain", "cmrRunStatus"), Restrictions.eq("code", "registered"));
		}

		TorRun example=new TorRun();
		example.setTorTypeStatus(torTypeStatus);
		example.setTorCompetition(torCmrCmn.getTorCompetition());
		List<TorRun> torRuns=daoFactory.getTorRunDAO().findByExample(example);
		for (TorRun torRun : torRuns) {
			if (daoFactory.getTorCmrCmnRunDAO().findByCriteria(Restrictions.eq("torRun", torRun)
						, Restrictions.eq("torCmrCmn",torCmrCmn)).size()==0) {
				TorCmrCmnRun torCmrCmnRun=new TorCmrCmnRun();
				torCmrCmnRun.setTorRun(torRun);
				torCmrCmnRun.setTorCmrCmn(torCmrCmn);
				torCmrCmnRun.setTorTypeStatus(ccrStatus);
				save(torCmrCmnRun);
			}
		}		
	}

	@Override
	public void generateAll(List<TorCmrCmn> torCmrCmns) {
		for (TorCmrCmn torCmrCmn : torCmrCmns) {
			generate(torCmrCmn);	
		}
	}
	
	public List<TorCmrCmnRun> reOrder(List<TorCmrCmnRun> torCmrCmnRuns, String algorithm) {
		
		List<TorCmrCmnRun> newList=new ArrayList<TorCmrCmnRun>();
		TorCmrCmnRun last=torCmrCmnRuns.get(0);
		newList.add(torCmrCmnRuns.get(0));
		torCmrCmnRuns.remove(0);
		Boolean rest=false;
		while (torCmrCmnRuns.size()>0 && !rest) {
			int index=0;
			while (index<torCmrCmnRuns.size() && last.getTorCmrCmn().getTorCompetitor().getTorCountry().getId().equals(torCmrCmnRuns.get(index).getTorCmrCmn().getTorCompetitor().getTorCountry().getId())) {
				index++;
			}
			if (index<torCmrCmnRuns.size()) {
				newList.add(torCmrCmnRuns.get(index));
				last=torCmrCmnRuns.get(index);
				torCmrCmnRuns.remove(index);
			} else {
				// we are at the end randomize rest
				rest=true;
			}
		}
		if (rest) {
			Random random=new Random();
			for (TorCmrCmnRun torCmrCmnRun : torCmrCmnRuns) {
				int randomIndex=random.nextInt(newList.size());
				newList.add(randomIndex, torCmrCmnRun);
			}
		}
		return newList;
	}

	private void generateStartTimesInternal(Date startTime, Integer[] seconds, List<TorCmrCmnRun> torCmrCmnRuns) {		
		int i=0;
		for (TorCmrCmnRun torCmrCmnRun : torCmrCmnRuns) {
			startTime=DateUtils.addMinutes(startTime, seconds[i]);
			torCmrCmnRun.setStartTime(startTime);
			save(torCmrCmnRun);
			i++;
			if (i==seconds.length) {
				i=0;
			}
		}
	}
	
	@Override
	public void generateStartTimes(List<TorRun> torRuns, String pattern) {
		// TODO runs must be form same competition, at least event
		Date startTime=torRuns.get(0).getStartTime();
		String[] patternSeconds=pattern.split(":");
		Integer[] seconds=new Integer[patternSeconds.length];
		for (int i=0; i<patternSeconds.length; i++) {
			seconds[i]=Integer.parseInt(patternSeconds[i]);
		}	
		List<TorCmrCmnRun> torCmrCmnRuns=daoFactory.getTorCmrCmnRunDAO().findByCriteria(Restrictions.in("torRun", torRuns));
		for (TorCmrCmnRun torCmrCmnRun : torCmrCmnRuns) {
			if (!torCmrCmnRun.getTorCmrCmn().getTorCompetitor().getTorType().getCode().equals("Ind")) {
				torCmrCmnRuns.remove(torCmrCmnRun);
			}
		}
		List<TorCmrCmnRun> newList=reOrder(torCmrCmnRuns, null);
		generateStartTimesInternal(startTime, seconds, newList);

	}


}