package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorRun;

public interface TorCmrCmnRunService extends DomainService<TorCmrCmnRun, Long>  {
	
	@Override
	public TorCmrCmnRun save(TorCmrCmnRun entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorCmrCmnRun> entities) throws ServiceException;
	
	
	public List<TorCmrCmnRun> getByExample(TorCmrCmnRun example);

	public List<TorCmrCmnRun> getByExample(TorCmrCmnRun example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	public List<TorCmrCmnRun> getByExampleCategory(TorCmrCmnRun example, TorCategory torCategory, Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	public int getByExampleSize(TorCmrCmnRun example);	
	
	public void generate(TorCmrCmn torCmrCmn);
	public void generateAll(List<TorCmrCmn> torCmrCmns);
	
	public void generateStartTimes(List<TorRun> torRuns, String pattern);
}