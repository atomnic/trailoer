package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorSolution;

public interface TorSolutionService extends DomainService<TorSolution, Long>  {
	
	@Override
	public TorSolution save(TorSolution entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorSolution> entities) throws ServiceException;
	
	
	public List<TorSolution> getByExample(TorSolution example, boolean solutionEntry);

	public List<TorSolution> getByExample(TorSolution example, boolean solutionEntry, Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorSolution example, boolean solutionEntry);
	
	public void generate(TorCmrCmnRun torCmrCmnRun);
	public void generateAll(List<TorCmrCmnRun> torCmrCmnRuns);

}