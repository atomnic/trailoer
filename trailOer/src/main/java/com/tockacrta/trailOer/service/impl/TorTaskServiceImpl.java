package com.tockacrta.trailOer.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.service.TorTaskService;

public class TorTaskServiceImpl extends BaseDomainServiceImpl<TorTask, Long> implements TorTaskService {

	public TorTaskServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorTaskDAO();
	}
	
	public List<TorTask> getByExample(TorTask example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorTask example) {
		return daoFactory.getTorTaskDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorTask> getByExample(TorTask example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorTaskDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorTask save(TorTask entity) throws ServiceException {
		String fp=String.format("%1$" + 2 + "s", entity.getNo()).replace(' ', '0');
		if (entity.getTorTask()!=null) {
			String sp=String.format("%1$" + 2 + "s", entity.getTorTask().getNo()).replace(' ', '0');
			entity.setFullNo(sp+"."+fp);
		} else {
			entity.setFullNo(fp);
		}
		TorTask ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorTask> entities) throws ServiceException {
		for (TorTask entity : entities) {
			save(entity);
		}
	}


}