package com.tockacrta.trailOer.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorComplains;

public interface TorComplainsService extends DomainService<TorComplains, Long>  {
	
	@Override
	public TorComplains save(TorComplains entity) throws ServiceException;
	
	@Override
	public void saveAll(Collection<TorComplains> entities) throws ServiceException;
	
	
	public List<TorComplains> getByExample(TorComplains example);

	public List<TorComplains> getByExample(TorComplains example,  Integer firstElement, Integer maxElements, Map<String, Boolean> sorts);
	
	public int getByExampleSize(TorComplains example);	
}