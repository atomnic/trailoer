package com.tockacrta.trailOer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.hibernate.service.spi.ServiceException;

import com.tockacrta.trailOer.model.TorTypes;
import com.tockacrta.trailOer.service.TorTypesService;

public class TorTypesServiceImpl extends BaseDomainServiceImpl<TorTypes, Long> implements TorTypesService {

	public TorTypesServiceImpl() {
		super();
	}
	
	@Override
	protected void setDefaultDAO() {
		super.defaultDAO = daoFactory.getTorTypesDAO();
	}
	
	public List<TorTypes> getByExample(TorTypes example) {
		return getByExample(example, 0, 0, null);
	}

	public int getByExampleSize(TorTypes example) {
		return daoFactory.getTorTypesDAO().findByExampleSize(example);
	}
	
	@Override
	public List<TorTypes> getByExample(TorTypes example, Integer firstElement, Integer maxElements, Map<String, Boolean> sort) {
		return daoFactory.getTorTypesDAO().findByExample(example, firstElement, maxElements, sort);
	}
	

	@Override
	public TorTypes save(TorTypes entity) throws ServiceException {
		TorTypes ret= super.save(entity);
		return ret;
	}
	
	@Override
	public void saveAll(Collection<TorTypes> entities) throws ServiceException {
		for (TorTypes entity : entities) {
			save(entity);
		}
	}

	@Override
	public TorTypes getByDomain(String domain, String code) {
		List<TorTypes> list= daoFactory.getTorTypesDAO().findByDomain(domain, code);
		if (list!=null && list.size()>0) {	// TODO think twice if there i spossibiliti thata exists two same codes for one domain
			return list.get(0);
		}
		return null;
	}

	

}