package com.tockacrta.trailOer.service.ranking.model;

import java.math.BigDecimal;
import java.util.Date;

import com.tockacrta.trailOer.model.TorCmrCmnRun;

public class PreOData {
	TorCmrCmnRun torCmrCmnRun;
	
	Date start;
	Date finish;
	Integer	time;
	BigDecimal tcTime;
	Integer penaltyPoints;
	Integer pointsAll;
	Integer points;
	
	public Date getStart() {
		return start;
	}
	public Date getFinish() {
		return finish;
	}
	public Integer getTime() {
		return time;
	}
	public Integer getPenaltyPoints() {
		return penaltyPoints;
	}
	public Integer getPointsAll() {
		return pointsAll;
	}
	public Integer getPoints() {
		return points;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public void setFinish(Date finish) {
		this.finish = finish;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public void setPenaltyPoints(Integer penaltyPoints) {
		this.penaltyPoints = penaltyPoints;
	}
	public void setPointsAll(Integer pointsAll) {
		this.pointsAll = pointsAll;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public TorCmrCmnRun getTorCmrCmnRun() {
		return torCmrCmnRun;
	}
	public void setTorCmrCmnRun(TorCmrCmnRun torCmrCmnRun) {
		this.torCmrCmnRun = torCmrCmnRun;
	}
	public BigDecimal getTcTime() {
		return tcTime;
	}
	public void setTcTime(BigDecimal tcTime) {
		this.tcTime = tcTime;
	}
	

}
