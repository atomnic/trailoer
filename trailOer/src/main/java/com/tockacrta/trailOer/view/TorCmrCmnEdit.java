package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.models.Selectable;
import com.tockacrta.trailOer.view.util.UIComponentUtils;

public class TorCmrCmnEdit extends BaseEdit<Selectable<TorCmrCmn>> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;
	
	protected TorCompetitorEdit master; 
	protected TorEvent torEvent;
	protected TorCategory torCategory;
	List<TorCompetition> torCompetitionsAll;
	List<TorCompetition> torCompetitionsSelected;
	List<TorCmrCmn> deletedRows=new ArrayList<TorCmrCmn>();
	public void setClazz() {
		Selectable<TorCmrCmn> tmp=new Selectable<TorCmrCmn>(null, false);
		this.clazz= (Class<Selectable<TorCmrCmn>>) tmp.getClass();
	}
	
	public Boolean masterize() {
		if (master!=null) { 
			if (master.getSelectedRow()!=null) {				
				return true;
			} else {			
				return false;
			}
		}	
		return false;
	}
	
    @PostConstruct
    public void init() {
    		
    	    torEvent=serviceFactory.getTorEventService().getOpen();
    		dataList= new LazyDataModel<Selectable<TorCmrCmn>>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;		
			
			@Override
		    public List<Selectable<TorCmrCmn>>  load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {				
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				if (masterize()) {				
					//currentPage=serviceFactory.getTorCmrCmnService().getByExample(example, first, pageSize, sorts);
					currentPage=serviceFactory.getTorCmrCmnService().getByCmrAndEvn(master.getSelectedRow(), torEvent, true);
				} else {
					headerSupp="";
					if (currentPage!=null) {
						currentPage.clear();
					} else {
						currentPage=new ArrayList<Selectable<TorCmrCmn>>();	
					}
				}	
				return currentPage;
			};		
			
			@Override			
			public Object getRowKey(Selectable<TorCmrCmn> object) {
				return object.getData().getId();
			}
    	};
    	dataList.setRowCount(serviceFactory.getTorCompetitionService().getByEvent(torEvent).size());
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	/*
    	if (currentPage!=null) {    	 
    		dataList.setRowCount(currentPage.size());
    	} else {
    		dataList.setRowCount(0);
    	}
    	*/
    }
    
    public String search() {
    	if (masterize()) {
    		dataList.setRowCount(serviceFactory.getTorCompetitionService().getByEvent(torEvent).size());
    	} else {
    		dataList.setRowCount(0);
    	}
    	return null;
    }
    
        
    public String deleteRow() {
    	// for unselected, service will delete row
    	return null;
    }

    public String save() {
    	try {
    		if (master!=null) {
    			master.save();
    		}
    		serviceFactory.getTorCmrCmnService().saveAll(currentPage);
    		changed=false;
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }

	public TorCompetitorEdit getMaster() {
		return master;
	}

	public void setMaster(TorCompetitorEdit master) {
		this.master = master;		
	}

	public TorEvent getTorEvent() {
		return torEvent;
	}

	public List<TorCompetition> getTorCompetitionsAll() {
		return torCompetitionsAll;
	}

	public List<TorCompetition> getTorCompetitionsSelected() {
		return torCompetitionsSelected;
	}

	public void setTorEvent(TorEvent torEvent) {
		this.torEvent = torEvent;
	}

	public void setTorCompetitionsAll(List<TorCompetition> torCompetitionsAll) {
		this.torCompetitionsAll = torCompetitionsAll;
	}

	public void setTorCompetitionsSelected(
			List<TorCompetition> torCompetitionsSelected) {
		this.torCompetitionsSelected = torCompetitionsSelected;
	}    
	 
	@Override
	public String getHeaderSupp() {
		if (master!=null && master.getSelectedRow()!=null) {
			headerSupp=master.getSelectedRow().getFirstName()+" "+master.getSelectedRow().getLastName();
		} else {
			headerSupp="";
		}
		return headerSupp;
	}
	 
}
