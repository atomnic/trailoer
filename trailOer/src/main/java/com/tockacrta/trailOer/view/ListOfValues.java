package com.tockacrta.trailOer.view;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorClub;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorCountry;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.model.TorTypes;
import com.tockacrta.trailOer.service.ServiceFactory;

public class ListOfValues {

	private ServiceFactory serviceFactory;
	
	
	public List<TorTask> getTorTasks() {
		List<TorTask> result;
		TorRun torRun=(TorRun)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("run");
		String typeCode=(String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("type");
		TorCategory torCategory=(TorCategory)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("category");
		TorTask example=new TorTask();
		example.setTorRun(torRun);
		example.setTorCategory(torCategory);
		if (typeCode!=null) {
			example.setTorTypeType(serviceFactory.getTorTypesService().getByDomain("taskType", typeCode));
		}
		result=serviceFactory.getTorTaskService().getByExample(example);
		return result;
	}
	
	public List<TorTypes> getTorTypess() {
		List<TorTypes> result;
		String domain=(String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("domain");
		TorTypes example=new TorTypes();
		example.setDomain(domain);
		result=serviceFactory.getTorTypesService().getByExample(example);
		return result;
	}

	public List<TorTypes> getTorTypess2() {
		List<TorTypes> result;
		String domain=(String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("domain");
		TorTypes example=new TorTypes();
		example.setDomain(domain);
		result=serviceFactory.getTorTypesService().getByExample(example);
		return result;
	}
	
	
	public List<TorCompetition> getTorCompetitionsByEvent() {
		TorEvent torEvent=(TorEvent)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("torEvent");
		List<TorCompetition> result;
		result=serviceFactory.getTorCompetitionService().getByEvent(torEvent);
		return result;
	}
	
	public List<TorClub> getTorClubs() {
		List<TorClub> result;
		result=serviceFactory.getTorClubService().getAll();
		return result;
	}
	
	public List<TorCompetition> getTorCompetitions() {
		List<TorCompetition> result;
		result=serviceFactory.getTorCompetitionService().getAll();
		return result;
	}
	
	public List<TorRun> getTorRunsByCompetition() {
		TorCompetition torCompetition=(TorCompetition)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("torCompetition");
		List<TorRun> result;
		TorRun example=new TorRun();
		example.setTorCompetition(torCompetition);
		result=serviceFactory.getTorRunService().getByExample(example);
		return result;
	}
	
	public List<TorCategory> getTorCategorys() {
		List<TorCategory> result;
		result=serviceFactory.getTorCategoryService().getAll();
		return result;
	}
	
	public List<TorCountry> getTorCountrys() {
		List<TorCountry> result;
		result=serviceFactory.getTorCountryService().getAll();
		return result;
	}
	
	public List<TorEvent> getTorEvents() {
		List<TorEvent> result;
		result=serviceFactory.getTorEventService().getAll();
		return result;
	}
	
	public List<TorCompetitor> getTorCompetitorTeams() {
		List<TorCompetitor> result;
		result=serviceFactory.getTorCompetitorService().getByType("Team", 0, 0, null);
		return result;
	}

	public ServiceFactory getServiceFactory() {
		return serviceFactory;
	}

	public void setServiceFactory(ServiceFactory serviceFactory) {
		this.serviceFactory = serviceFactory;
	}
}
