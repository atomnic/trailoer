package com.tockacrta.trailOer.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.model.TorCategory;


public class TorCategoryConverter extends BaseConverter implements Converter {
	private static Log log = LogFactory.getLog(TorCategoryConverter.class);

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		Object result = null;
		if (!StringUtils.isEmpty(value)) {
			DaoFactory daoFactory = getDaoFactory(context);
			result = daoFactory.getTorCategoryDAO().findById(Long.valueOf(value));
		}
		return result;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return StringUtils.EMPTY;
		}
		else if (value instanceof TorCategory) {
			TorCategory torCategory = (TorCategory) value;
			if (torCategory.getId() == null) {
				return StringUtils.EMPTY;
			}
			return torCategory.getId().toString();
		}
		else {
			log.error("TorCategorynConverter object value not instance of TorCategory: "+value.getClass().toString());
			return StringUtils.EMPTY;
		}
	}

}