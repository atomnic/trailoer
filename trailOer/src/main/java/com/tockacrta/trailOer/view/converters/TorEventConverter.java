package com.tockacrta.trailOer.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.model.TorEvent;


public class TorEventConverter extends BaseConverter implements Converter {
	private static Log log = LogFactory.getLog(TorEventConverter.class);

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		Object result = null;
		if (!StringUtils.isEmpty(value)) {
			DaoFactory daoFactory = getDaoFactory(context);
			result = daoFactory.getTorEventDAO().findById(Long.valueOf(value));
		}
		return result;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return StringUtils.EMPTY;
		}
		else if (value instanceof TorEvent) {
			TorEvent event = (TorEvent) value;
			if (event.getId() == null) {
				return StringUtils.EMPTY;
			}
			return event.getId().toString();
		}
		else {
			log.error("TorEventConverter wrong usage for: "+value.getClass().toString());
			return StringUtils.EMPTY;
		}
	}

}