package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tockacrta.trailOer.model.TorTypes;

public class TorTypesEdit extends BaseEdit<TorTypes> implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -830083775822947847L;
	
	@Override
	public void setClazz() {
		clazz=TorTypes.class;		
	}

    @PostConstruct
    public void init() {
    	example=new TorTypes();
    	dataList = new LazyDataModel<TorTypes>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorTypes> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorTypesService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorTypesService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorTypesService().getByExampleSize(example));
    	return null;
    }
        
    public String deleteRow() {
    	serviceFactory.getTorTypesService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		serviceFactory.getTorTypesService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }	 
	
}
