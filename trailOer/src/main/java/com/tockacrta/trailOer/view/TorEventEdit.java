package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tockacrta.trailOer.model.TorEvent;

public class TorEventEdit extends BaseEdit<TorEvent> implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -830083775822947847L;
	
	@Override
	public void setClazz() {
		clazz=TorEvent.class;		
	}

    @PostConstruct
    public void init() {
    	example=new TorEvent();
    	dataList = new LazyDataModel<TorEvent>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorEvent> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorEventService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorEventService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorEventService().getByExampleSize(example));
    	return null;
    }
        
    public String deleteRow() {
    	serviceFactory.getTorEventService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		serviceFactory.getTorEventService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }	 
	
}
