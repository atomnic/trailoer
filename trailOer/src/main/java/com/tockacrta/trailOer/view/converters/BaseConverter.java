package com.tockacrta.trailOer.view.converters;

import javax.faces.context.FacesContext;

import org.springframework.web.jsf.FacesContextUtils;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.service.ServiceFactory;

public abstract class BaseConverter {
	/**
	 * Vraca serviceFactory bean, kojeg preuzima iz konteksta jer ga nije moguce postaviti preko
	 * settera u faces-config.xml
	 * 
	 * @param context
	 * @return
	 */
	protected ServiceFactory getServiceFactory(FacesContext context) {
		return (ServiceFactory) FacesContextUtils.getWebApplicationContext(context).getBean("serviceFactory", ServiceFactory.class);
	}
	
	/**
	 * Vraca daoFactory bean kojeg preuzima iz konteksta jer ga nije moguce postaviti preko
	 * settera u faces-config.xml
	 * 
	 * <b>Ovu metodu treba oznaciti @deprecated nakon sto se kreiraju sve service klase, pa nece biti vise potrebe 
	 * da view layer pristupa persistence layeru!!!</b>
	 * 
	 * @param context
	 * @return
	 */
	protected DaoFactory getDaoFactory(FacesContext context) {
		return (DaoFactory) FacesContextUtils.getWebApplicationContext(context).getBean("daoFactory", DaoFactory.class);
	}
}
