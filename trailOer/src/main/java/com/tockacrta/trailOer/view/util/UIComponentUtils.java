package com.tockacrta.trailOer.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

public class UIComponentUtils {
	public static DataTable findFirstDatTableParent(UIComponent uic) {
		UIComponent parent=UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getParent();
		while  (parent!=null && !(parent instanceof DataTable)) {
			parent=parent.getParent();
		}
		return (DataTable) parent;
	}
}
