package com.tockacrta.trailOer.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorTask;


public class TorTaskConverter extends BaseConverter implements Converter {
	private static Log log = LogFactory.getLog(TorCompetitionConverter.class);

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		Object result = null;
		if (!StringUtils.isEmpty(value)) {
			DaoFactory daoFactory = getDaoFactory(context);
			result = daoFactory.getTorTaskDAO().findById(Long.valueOf(value));
		}
		return result;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return StringUtils.EMPTY;
		}
		else if (value instanceof TorTask) {
			TorTask torTask = (TorTask) value;
			if (torTask.getId() == null) {
				return StringUtils.EMPTY;
			}
			return torTask.getId().toString();
		}
		else {
			log.error("TorTaskConverter object value not instance of TorTask: "+value.getClass().toString());
			return StringUtils.EMPTY;
		}
	}

}