package com.tockacrta.trailOer.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;

import java.util.Map;

import com.tccrt.chemistryPdf.document.BaseDocument;
import com.tccrt.chemistryPdf.document.DocumentFactory;
import com.tccrt.chemistryPdf.document.impl.PDDocumentImpl;
import com.tccrt.chemistryPdf.model.Atom;
import com.tccrt.chemistryPdf.model.Chemistry;
import com.tccrt.chemistryPdf.model.Compound;
import com.tccrt.chemistryPdf.model.DateAtom;
import com.tccrt.chemistryPdf.model.Laboratory.FlavorTypes;
import com.tccrt.chemistryPdf.model.StringAtom;
import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.reports.model.TorCmrCmnRunAtom;

public class TorCmrCmnRunEdit extends BaseEdit<TorCmrCmnRun> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	protected TorRunEdit master; 
	private DocumentFactory documentFactory;
	private TorCategory torCategoryPrint;
	
	public void setClazz() {
		this.clazz=TorCmrCmnRun.class;
	}
	
	public Boolean masterize() {
		if (master!=null) { 
			if (master.getSelectedRow()!=null) {
				example.setTorRun(master.getSelectedRow());
			} else {			
				return false;
			}
		}	
		return true;
	}
	
    @PostConstruct
    public void init() {
    		example=new TorCmrCmnRun();
    		example.setTorCmrCmn(new TorCmrCmn());
    		example.getTorCmrCmn().setTorCompetitor(new TorCompetitor());
    		dataList= new LazyDataModel<TorCmrCmnRun>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;		
			
			@Override
		    public List<TorCmrCmnRun> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {				
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortField!=null) { //   sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, sortOrder.compareTo(SortOrder.ASCENDING)==0);
				}
				if (masterize()) {
					currentPage=serviceFactory.getTorCmrCmnRunService().getByExample(example, first, pageSize, sorts);
				} else {
					if (currentPage!=null) {
						currentPage.clear();
					} else {
						currentPage=new ArrayList<TorCmrCmnRun>();
					}
				}
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorCmrCmnRunService().getByExampleSize(example)); 	
    }
    
    public String search() {
    	if (masterize()) {
    		dataList.setRowCount(serviceFactory.getTorCmrCmnRunService().getByExampleSize(example));
    	} else {
    		dataList.setRowCount(0);
    	}
    	return null;
    }
    
    
    @Override
    public String createRow() {
    	if (master!=null && master.getSelectedRow()!=null) {
        	super.createRow();
    		newRow.setTorRun(master.getSelectedRow());
    	} else if (master.getSelectedRow()==null) {
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("First choose stage/run!", null);
    	}
    	return null;
    };
    
    public String deleteRow() {
    	serviceFactory.getTorCmrCmnRunService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		if (master!=null) {
    			master.save();
    		}
    		serviceFactory.getTorCmrCmnRunService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    // Part for generating PDF and returning streamed cont.
    private StreamedContent file;
    
    public StreamedContent getPrint() {  // TODO this has to go to service layer, for now just here for test purposes
    	// Check, always refresh, or with each change on start list regenerate document
    	BaseDocument startList= documentFactory.getDocument("startList");
		Compound startListCompund= (Compound) startList.getChemistry().get("nameColumn");
		Compound startTimeColumn= (Compound) startList.getChemistry().get("startTimeColumn");
		Compound clubColumn= (Compound) startList.getChemistry().get("clubColumn");
		startListCompund.clear();
		startTimeColumn.clear();
		clubColumn.clear();
		startListCompund.setValue(master.getSelectedRow().getNameInt()+"     Category: "+torCategoryPrint.getName());
		startListCompund.setX(50F);
		
		// Another row before actual names list
		DateAtom startDate=new DateAtom(master.selectedRow.getStartTime());
		startDate.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		startDate.addFlavor(FlavorTypes.FontSize, 14F);
		
		startDate.setX(0F);
		startDate.setY(-15F);
		startListCompund.add(startDate);
		//
		
		// Another row before actual start times list		
		StringAtom dateAtomHeader=new StringAtom("Start");
		// we dont need flavor 
		dateAtomHeader.setX(0F);
		dateAtomHeader.setY(-15F);
		startTimeColumn.add(dateAtomHeader);
		
		// Another row before actual start times list		
		StringAtom clubHeader=new StringAtom("club");
		// we dont need flavor 
		clubHeader.setX(0F);
		clubHeader.setY(-15F);
		clubColumn.add(clubHeader);
		//
		
    	HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
    	sorts.put("ccr.startTime", true);
    	for (TorCmrCmnRun ccr : serviceFactory.getTorCmrCmnRunService().getByExampleCategory(example, torCategoryPrint, 0, 0, sorts)) {    
//    		Compound row=new Compound();
//    		row.setY(-15F);
//    		row.setX(0F);
//    		row.setAbsPositioning(true);
    		
    		TorCmrCmnRunAtom atom=new TorCmrCmnRunAtom(ccr);
    		atom.setAbsPositioning(false);
    		atom.setX(0F);
    		atom.setY(-15F);  // relativ TODO consider autoSpill flavor, so that offset is calculated
    		startListCompund.add(atom);
   // 		row.add(atom);
    		
    		DateAtom dateAtom=new DateAtom(ccr.getStartTime());
    		dateAtom.setFormat("H:mm");
    		dateAtom.setX(0F);
    		dateAtom.setY(-15F);
  //  		row.add(dateAtom);
    		startTimeColumn.add(dateAtom);
    		
    		String club=""; 
    		if (ccr.getTorCmrCmn().getTorCompetitor().getTorClub()!=null) {
    			club=ccr.getTorCmrCmn().getTorCompetitor().getTorClub().getName();
    		}
    		
    		StringAtom clubAtom=new StringAtom(club);
    		dateAtom.setFormat("H:mm");
    		clubAtom.setX(0F);
    		clubAtom.setY(-15F);
  //  		row.add(dateAtom);
    		clubColumn.add(clubAtom);
		}    	
    	startList.init();
    	startList.spill();
    	startList.save();
        InputStream stream;
		try {
			stream = new FileInputStream(startList.getFileLocation()+startList.getFileName());
	        file = new DefaultStreamedContent(stream, "application/pdf", startList.getFileName());    	
	        return file;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

	public TorRunEdit getMaster() {
		return master;
	}

	public void setMaster(TorRunEdit master) {
		this.master = master;		
	}

	public DocumentFactory getDocumentFactory() {
		return documentFactory;
	}

	public void setDocumentFactory(DocumentFactory documentFactory) {
		this.documentFactory = documentFactory;
	}

	public TorCategory getTorCategoryPrint() {
		return torCategoryPrint;
	}

	public void setTorCategoryPrint(TorCategory torCategoryPrint) {
		this.torCategoryPrint = torCategoryPrint;
	}
	 
}
