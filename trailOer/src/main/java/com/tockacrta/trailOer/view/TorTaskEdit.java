package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorTask;

public class TorTaskEdit extends BaseEdit<TorTask> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	protected TorRunEdit master; 
	protected TorCategory torCategory;
	
	public void setClazz() {
		this.clazz=TorTask.class;
	}
	
	public Boolean masterize() {
		if (master!=null) { 
			if (master.getSelectedRow()!=null) {
				example.setTorRun(master.getSelectedRow());
			} else {			
				return false;
			}
		}	
		return true;
	}
	
    @PostConstruct
    public void init() {
    		example=new TorTask();
    		dataList= new LazyDataModel<TorTask>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;		
			
			@Override
		    public List<TorTask> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {				
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				sorts.put("tsk.fullNo", true);
				if (masterize()) {
					currentPage=serviceFactory.getTorTaskService().getByExample(example, first, pageSize, sorts);
				} else {
					if (currentPage!=null) {
						currentPage.clear();
					} else {
						currentPage=new ArrayList<TorTask>();
					}
				}
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorTaskService().getByExampleSize(example)); 	
    }
    
    public String search() {
    	if (masterize()) {
    		dataList.setRowCount(serviceFactory.getTorTaskService().getByExampleSize(example));
    	} else {
    		dataList.setRowCount(0);
    	}
    	return null;
    }
    
    
    @Override
    public String createRow() {
    	if (master!=null && master.getSelectedRow()!=null) {
        	super.createRow();
        	newRow.setTorCategory(torCategory);
    		newRow.setTorRun(master.getSelectedRow());
    	} else if (master.getSelectedRow()==null) {
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("First choose stage/run!", null);
    	}
    	return null;
    };
    
    public String deleteRow() {
    	serviceFactory.getTorTaskService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		if (master!=null) {
    			master.save();
    		}
    		serviceFactory.getTorTaskService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }

	public TorRunEdit getMaster() {
		return master;
	}

	public void setMaster(TorRunEdit master) {
		this.master = master;		
	}

	public TorCategory getTorCategory() {
		return torCategory;
	}

	public void setTorCategory(TorCategory torCategory) {
		this.torCategory = torCategory;
	}
	 
}
