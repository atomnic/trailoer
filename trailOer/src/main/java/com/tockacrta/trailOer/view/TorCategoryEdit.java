package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCategory;

public class TorCategoryEdit extends BaseEdit<TorCategory> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	
	public void setClazz() {
		this.clazz=TorCategory.class;
	}
	
    @PostConstruct
    public void init() {
    		example=new TorCategory();
    		dataList= new LazyDataModel<TorCategory>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorCategoryService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorCategoryService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorCategoryService().getByExampleSize(example));
    	return null;
    }
    
    
    public String deleteRow() {
    	serviceFactory.getTorCategoryService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		serviceFactory.getTorCategoryService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }    
	 
	 
}
