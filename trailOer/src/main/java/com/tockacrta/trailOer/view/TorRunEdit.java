package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;

public class TorRunEdit extends BaseEdit<TorRun> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	protected TorCompetitionEdit master; 
	protected TorEvent torEvent;
	protected TorCompetition torCompetition;
	
	public void setClazz() {
		this.clazz=TorRun.class;
	}
	
	public Boolean masterize() {
		if (master!=null) { 
			if (master.getSelectedRow()!=null) {
				example.setTorCompetition(master.getSelectedRow());
			} else {			
				return false;
			}
		}else {			
			example.setTorCompetition(torCompetition);			
			if (torEvent!=null) {
				if (example.getTorCompetition()==null) {
		    		torCompetition=new TorCompetition();
		    		example.setTorCompetition(torCompetition);
				}
				torCompetition.setTorEvent(torEvent);
			}
		}
		return true;
	}
	
    @PostConstruct
    public void init() {
    		example=new TorRun();
    		torEvent=new TorEvent();
    		torCompetition=new TorCompetition();
    		dataList= new LazyDataModel<TorRun>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;		
			
			@Override
		    public List<TorRun> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {				
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				if (masterize()) {					
					currentPage=serviceFactory.getTorRunService().getByExample(example, first, pageSize, sorts);
				} else {
					if (currentPage!=null) {
						currentPage.clear();
					} else {
						currentPage=new ArrayList<TorRun>();
					}
				}
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorRunService().getByExampleSize(example)); 	
    }
    
    public String search() {
    	if (masterize()) {
    		dataList.setRowCount(serviceFactory.getTorRunService().getByExampleSize(example));
    	} else {
    		dataList.setRowCount(0);
    	}
    	return null;
    }
    
    
    @Override
    public String createRow() {
    	if (master!=null && master.getSelectedRow()!=null) {
        	super.createRow();
    		newRow.setTorCompetition(master.getSelectedRow());
    	} else if (master.getSelectedRow()==null) {
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("First choose competition!", null);
    	}
    	return null;
    };
    
    public String deleteRow() {
    	serviceFactory.getTorRunService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		if (master!=null) {
    			master.save();
    		}
    		serviceFactory.getTorRunService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    public String generateStartTimes() {
    	List<TorRun> torRuns;
    	if (getSelectedRow()!=null ) {
    		if (selectedRow.getStartTimeGroup()!=null) {
    			example.setStartTimeGroup(selectedRow.getStartTimeGroup());
    			torRuns=serviceFactory.getTorRunService().getByExample(example);
    			// TODO must be from same event (not necessary competition) and same type
    		} else {
    			torRuns=new ArrayList<TorRun>();
    			torRuns.add(getSelectedRow());
    		}
    		// TODO select pattern from selectOneMeny 
    		serviceFactory.getTorCmrCmnRunService().generateStartTimes(torRuns, "2:2:2");    		
    	}
    	///serviceFactory.getTorCmrCmnRunService().generateStartTimes(torRuns, "2:2:4");
    	return null;
    }


	public TorCompetitionEdit getMaster() {
		return master;
	}

	public void setMaster(TorCompetitionEdit master) {
		this.master = master;		
	}

	public TorEvent getTorEvent() {
		return torEvent;
	}

	public TorCompetition getTorCompetition() {
		return torCompetition;
	}

	public void setTorEvent(TorEvent torEvent) {
		this.torEvent = torEvent;
	}

	public void setTorCompetition(TorCompetition torCompetition) {
		this.torCompetition = torCompetition;
	}    
	 
	 
}
