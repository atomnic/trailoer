package com.tockacrta.trailOer.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;

import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.tccrt.chemistryPdf.document.BaseDocument;
import com.tccrt.chemistryPdf.document.DocumentFactory;
import com.tccrt.chemistryPdf.model.Compound;
import com.tccrt.chemistryPdf.model.DateAtom;
import com.tccrt.chemistryPdf.model.DecimalAtom;
import com.tccrt.chemistryPdf.model.StringAtom;
import com.tccrt.chemistryPdf.model.Laboratory.FlavorTypes;
import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.reports.model.TorCmrCmnRunAtom;
import com.tockacrta.trailOer.service.ranking.model.PreOData;

public class Reports extends BaseEdit<TorCmrCmnRun> implements Serializable{
	private TorEvent torEvent;
	private TorCompetition torCompetition;
	private TorRun torRun;
	private TorCategory torCategory;
	private String team="";
	private String uri;
	
	private DocumentFactory documentFactory;
	
	public TorEvent getTorEvent() {
		return torEvent;
	}
	public TorCompetition getTorCompetition() {
		return torCompetition;
	}
	public TorRun getTorRun() {
		return torRun;
	}
	public TorCategory getTorCategory() {
		return torCategory;
	}
	public void setTorEvent(TorEvent torEvent) {
		this.torEvent = torEvent;
	}
	public void setTorCompetition(TorCompetition torCompetition) {
		this.torCompetition = torCompetition;
	}
	public void setTorRun(TorRun torRun) {
		this.torRun = torRun;
	}
	public void setTorCategory(TorCategory torCategory) {
		this.torCategory = torCategory;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	
	private StreamedContent file;
    public StreamedContent getPrint() {  // TODO this has to go to service layer, for now just here for test purposes
    	// Check, always refresh, or with each change on start list regenerate document
    	BaseDocument startList= documentFactory.getDocument("preOPreliminaryList");
    	startList.getChemistry().remove("finalPointsColumn"); // we will add if necessary
		Compound startListCompund= (Compound) startList.getChemistry().get("nameColumn");
		Compound no= new Compound();		
		Compound startTimeColumn= new Compound();		
		Compound finishTimeColumn= new Compound();
		Compound cnyColumn=new Compound(); 
		Compound timeColumn= new Compound();
		Compound finalPointsColumn=null;
		if (torRun.getTorTypes().getCode().equals("P")) {
			finalPointsColumn= new Compound();
		}
		startListCompund.clear();
		
		
		no.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		no.addFlavor(FlavorTypes.FontSize, 14F);
		no.setAbsPositioning(true);		
		no.setX(50F);
		no.setY(745F);
		startList.getChemistry().put("no", no);
		
		startTimeColumn.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		startTimeColumn.addFlavor(FlavorTypes.FontSize, 14F);
		startTimeColumn.setAbsPositioning(true);		
		startTimeColumn.setX(300F);
		startTimeColumn.setY(760F);
		// startList.getChemistry().put("startTimeColumne", startTimeColumn);
		
		finishTimeColumn.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		finishTimeColumn.addFlavor(FlavorTypes.FontSize, 14F);
		finishTimeColumn.setAbsPositioning(true);		
		finishTimeColumn.setX(300F);
		finishTimeColumn.setY(760F);
		// startList.getChemistry().put("finishTimeColumn", finishTimeColumn);
		
		cnyColumn.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		cnyColumn.addFlavor(FlavorTypes.FontSize, 14F);
		cnyColumn.setAbsPositioning(true);		
		cnyColumn.setX(330F);
		cnyColumn.setY(745F);
		startList.getChemistry().put("cnyColumn", cnyColumn);
		
		timeColumn.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		timeColumn.addFlavor(FlavorTypes.FontSize, 14F);
		timeColumn.setAbsPositioning(true);		
		timeColumn.setX(400F);
		timeColumn.setY(745F);
		startList.getChemistry().put("timeColumn", timeColumn);
		
		if (torRun.getTorTypes().getCode().equals("P") && finalPointsColumn!=null) {
			finalPointsColumn.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
			finalPointsColumn.addFlavor(FlavorTypes.FontSize, 14F);
			finalPointsColumn.setAbsPositioning(true);		
			finalPointsColumn.setX(460F);
			finalPointsColumn.setY(745F);
			
			StringAtom finalPHeader=new StringAtom("Points");
			// we dont need flavor 
			finalPHeader.setX(0F);
			finalPHeader.setY(-15F);
			finalPointsColumn.add(finalPHeader);
			
			startList.getChemistry().put("finalPointsColumn", finalPointsColumn);
		}
		startListCompund.setValue(torRun.getNameInt()+"     Category: "+torCategory.getName());
		
		// Another row before actual names list
		DateAtom startDate=new DateAtom(torRun.getStartTime());
		startDate.addFlavor(FlavorTypes.FontStyle, PDType1Font.HELVETICA_BOLD);
		startDate.addFlavor(FlavorTypes.FontSize, 14F);
		
		startDate.setX(0F);
		startDate.setY(-15F);
		startListCompund.add(startDate);
		StringAtom nameHeader=new StringAtom("Name");
		// we dont need flavor 
		nameHeader.setX(0F);
		nameHeader.setY(-15F);
		startListCompund.add(nameHeader);
		//
		
		// Another row before actual start times list		
		StringAtom posHeader=new StringAtom("Rank");
		posHeader.setX(0F);
		posHeader.setY(-15F);
		no.add(posHeader);
		
		// Another row before actual start times list		
		StringAtom dateAtomHeader=new StringAtom("Start");
		dateAtomHeader.setX(0F);
		dateAtomHeader.setY(-15F);
		startTimeColumn.add(dateAtomHeader);
		
		// Another row before actual start times list		
		StringAtom finishHeader=new StringAtom("Finish"); 
		finishHeader.setX(0F);
		finishHeader.setY(-15F);
		finishTimeColumn.add(finishHeader);
		
		StringAtom cnyHeader=new StringAtom("Country");
		cnyHeader.setX(0F);
		cnyHeader.setY(-15F);
		cnyColumn.add(cnyHeader);		
				
		StringAtom timehHeader=new StringAtom("Time");
		timehHeader.setX(0F);
		timehHeader.setY(-15F);
		timeColumn.add(timehHeader);
		//
		//
		//

		
    	HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
    	Integer pos=1;
    	for (PreOData pod : serviceFactory.getPreORankingService().preOPreliminary(null, null, torRun, torCategory)) { //getTorCmrCmnRunService().getByExampleCategory(example, torCategoryPrint, 0, 0, sorts)) {    
//    		Compound row=new Compound();
//    		row.setY(-15F);
//    		row.setX(0F);
//    		row.setAbsPositioning(true);
    		StringAtom posAtom=new StringAtom(pos.toString());
    		posAtom.setAbsPositioning(false);
    		posAtom.setX(0F);
    		posAtom.setY(-15F);  // relativ TODO consider autoSpill flavor, so that offset is calculated
    		no.add(posAtom);    		
    		pos++;
    		
    		TorCmrCmnRunAtom atom=new TorCmrCmnRunAtom(pod.getTorCmrCmnRun());
    		atom.setAbsPositioning(false);
    		atom.setX(0F);
    		atom.setY(-15F);  // relativ TODO consider autoSpill flavor, so that offset is calculated
    		startListCompund.add(atom);
   // 		row.add(atom);
    		
    		StringAtom cnyAtom=new StringAtom(pod.getTorCmrCmnRun().getTorCmrCmn().getTorCompetitor().getTorCountry().getIso3Code());
    		cnyAtom.setX(0F);
    		cnyAtom.setY(-15F);
  //  		row.add(dateAtom);
    		cnyColumn.add(cnyAtom);    		
    		
    		DateAtom dateAtom=new DateAtom(pod.getTorCmrCmnRun().getStartTime());  // TODO actual start time
    		dateAtom.setFormat("H:mm");
    		dateAtom.setX(0F);
    		dateAtom.setY(-15F);
  //  		row.add(dateAtom);
    		startTimeColumn.add(dateAtom);
    		
    		DateAtom dateAtomFinish=new DateAtom(pod.getTorCmrCmnRun().getFinishTime());  // TODO actual start time
    		dateAtomFinish.setFormat("H:mm");
    		dateAtomFinish.setX(0F);
    		dateAtomFinish.setY(-15F);
  //  		row.add(dateAtom);
    		finishTimeColumn.add(dateAtomFinish);    		
    		
    		StringAtom timeAtom=new StringAtom(pod.getTcTime().toString());
    		timeAtom.setX(0F);
    		timeAtom.setY(-15F);
  //  		row.add(dateAtom);
    		timeColumn.add(timeAtom);
    		
    		if (torRun.getTorTypes().getCode().equals("P")) {
	    		StringAtom pointsAtom=new StringAtom(pod.getPointsAll().toString());
	    		pointsAtom.setX(0F);
	    		pointsAtom.setY(-15F);
	  //  		row.add(dateAtom);
	    		finalPointsColumn.add(pointsAtom);
    		}
    	}    	
    	startList.init();
    	startList.spill();
    	startList.save();
        InputStream stream;
		try {
			stream = new FileInputStream(startList.getFileLocation()+startList.getFileName());
	        file = new DefaultStreamedContent(stream, "application/pdf", startList.getFileName());    	
	        return file;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setClazz() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String deleteRow() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String save() {
		// TODO Auto-generated method stub
		return null;
	}
	public DocumentFactory getDocumentFactory() {
		return documentFactory;
	}
	public void setDocumentFactory(DocumentFactory documentFactory) {
		this.documentFactory = documentFactory;
	}	
	
}
