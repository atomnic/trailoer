package com.tockacrta.trailOer.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.model.TorTypes;


public class TorTypesConverter extends BaseConverter implements Converter {
	private static Log log = LogFactory.getLog(TorTypesConverter.class);

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		Object result = null;
		if (!StringUtils.isEmpty(value)) {
			DaoFactory daoFactory = getDaoFactory(context);
			result = daoFactory.getTorTypesDAO().findById(Long.valueOf(value));
		}
		return result;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return StringUtils.EMPTY;
		}
		else if (value instanceof TorTypes) {
			TorTypes event = (TorTypes) value;
			if (event.getId() == null) {
				return StringUtils.EMPTY;
			}
			return event.getId().toString();
		}
		else {
			log.error("TorTypeConverter wrong usage for: "+value.getClass().toString());
			return StringUtils.EMPTY;
		}
	}

}