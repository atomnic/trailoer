package com.tockacrta.trailOer.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.RowEditEvent;
import org.primefaces.model.LazyDataModel;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.tockacrta.trailOer.service.ServiceFactory;


public abstract class BaseEdit<T> {
	
	protected Class<T> clazz;
	
	protected ServiceFactory serviceFactory;
	
	protected T example;
	protected T selectedRow;
	protected T newRow;
	
	protected LazyDataModel<T> dataList;	
	protected List<T> currentPage;
	protected List<T> changedRows=new ArrayList<T>();
	
	protected Boolean changed=false;
	
	protected String headerSupp;
		
	
	public BaseEdit() {
		super();
		setClazz();
	}

	@PostConstruct
    public abstract void init(); 
    
    public abstract void  setClazz();
    
    public void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Action successfull.",  message) );
    }
    
    public void addError(String message, Exception ex) {
        FacesContext context = FacesContext.getCurrentInstance();
        String messageEx="";
        if (ex!=null) {
        	messageEx=ex.getMessage();
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, messageEx) );
    }
	
    public String createRow() {
    	try {
			newRow=clazz.newInstance();
	    	changedRows.add(newRow);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public void createRowListener(AjaxBehaviorEvent event) {
    	createRow();
    }
    
    public void cancelCreateRow() {
    	changedRows.remove(newRow);
    	newRow=null;    
    }
    
    public void cancelCreateRowListener(AjaxBehaviorEvent event) {
    	cancelCreateRow();
    }
    
    public  String search(){
    	selectedRow=null;
    	return null;
    };    
    
    public void searchListener(AjaxBehaviorEvent event) {
    	search();
    }
    
    public abstract String deleteRow();
    
    public void deleteRowListener(AjaxBehaviorEvent event) {
    	deleteRow();
    }
    
    public abstract String save();
    
    public void rowEditCompleteListener(RowEditEvent event) {
    	changed=true;
    	if (changedRows==null) changedRows=new ArrayList<T>();
    	changedRows.add((T) event.getObject());
    }
    
    public void rowEditInitListener(AjaxBehaviorEvent event) {
    }
    
    public void rowEditCancelListener(AjaxBehaviorEvent event) {    	
    }
    
    
	public ServiceFactory getServiceFactory() {
		return serviceFactory;
	}
	
	public void setServiceFactory(ServiceFactory serviceFactory) {
		this.serviceFactory = serviceFactory;
	}

	public LazyDataModel<T> getDataList() {
		return dataList;
	}

	public void setDataList(LazyDataModel<T> dataList) {
		this.dataList = dataList;
	}

	public T getSelectedRow() {
		return selectedRow;
	}

	public void setSelectedRow(T selectedRow) {
		this.selectedRow = selectedRow;
	}

	public Boolean getChanged() {
		return changed;
	}

	public void setChanged(Boolean changed) {
		this.changed = changed;
	}

	public T getNewRow() {
		return newRow;
	}

	public void setNewRow(T newRow) {
		this.newRow = newRow;
	}
	
	public T getExample() {
		return example;
	}

	public void setExample(T example) {
		this.example = example;
	}

	public List<T> getChangedRows() {
		return changedRows;
	}

	public void setChangedRows(List<T> changedRows) {
		this.changedRows = changedRows;
	}

	public String getHeaderSupp() {
		return headerSupp;
	}

	public void setHeaderSupp(String headerSupp) {
		this.headerSupp = headerSupp;
	}
}
