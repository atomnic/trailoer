package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCountry;

public class TorCountryEdit extends BaseEdit<TorCountry> implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -2184957175458733222L;

	@Override
	public void setClazz() {
		clazz=TorCountry.class;
	};

	@PostConstruct
    public void init() {
		example=new TorCountry();
    	dataList = new LazyDataModel<TorCountry>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorCountry> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorCountryService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorCountryService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorCountryService().getByExampleSize(example));
    	return null;
    }
       
    public String deleteRow() {
    	serviceFactory.getTorCountryService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		serviceFactory.getTorCountryService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }
	 
}
