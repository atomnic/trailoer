package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.RowEditEvent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.sun.faces.facelets.tag.jsf.core.AjaxHandler;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorCountry;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.service.ServiceFactory;
import com.tockacrta.trailOer.service.TorCountryService;

public class TorCompetitorEdit extends BaseEdit<TorCompetitor> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	//private TorCompetitor example=new TorCompetitor();
	//private TorCompetitor selectedRow;
	//private TorCompetitor newRow;
	//private LazyDataModel<TorCompetitor> torCompetitors;	
	//private List<TorCompetitor> currentPage;
	//private List<TorCompetitor> changedRows;
	
	private Boolean changed=false;
	private TorEvent torEvent;
	private List<TorCompetition> suggestedTorCompetitions;
	
	public void setClazz() {
		this.clazz=TorCompetitor.class;
	}
	
    @PostConstruct
    public void init() {
    	suggestedTorCompetitions=new ArrayList<TorCompetition>();
    	torEvent=serviceFactory.getTorEventService().getOpen();
   		example=new TorCompetitor();
   		dataList= new LazyDataModel<TorCompetitor>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorCompetitor> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorCompetitorService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorCompetitorService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorCompetitorService().getByExampleSize(example));
    	return super.search();
    }
    
    
    public String deleteRow() {
    	serviceFactory.getTorCompetitorService().delete(selectedRow);
    	return null;
    }
    

    public String save() {
    	try {
    		serviceFactory.getTorCompetitorService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }

	public List<TorCompetition> getSuggestedTorCompetitions() {
		return suggestedTorCompetitions;
	}

	public void setSuggestedTorCompetitions(List<TorCompetition> suggestedTorCompetitions) {
		this.suggestedTorCompetitions = suggestedTorCompetitions;
	}    
	 
	 
}
