package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.support.SecurityContextProvider;

import com.tockacrta.trailOer.model.TorCountry;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.service.ServiceFactory;

public class Main implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5068666424387078658L;
	private String first="Prvi tekst";
	private ServiceFactory serviceFactory;
	private TorCountry torCountry;
	private Long id;
	
	private final TimeZone timeZone=TimeZone.getTimeZone("CET");	// TODO set time zone into properties
	
	private String mainView="/blank.xhtml";
	
	public String testGet() {
		torCountry=serviceFactory.getTorCountryService().get(id);
		return null;
	}

	public String testSave() {
		/*
		TorCountry torCountry=new TorCountry();
		torCountry.setCreatedBy("AKA");
		torCountry.setDateCreated(new Date());
		torCountry.setIso2Code("SL");
		torCountry.setIso3Code("SLO");
		torCountry.setLocalCode("SLV");
		torCountry.setNameInt("Slovenia");
		torCountry.setNameLocal("Slovenija");
		*/

		serviceFactory.getTorCountryService().save(torCountry);
		return null;
	}
	
	
	public String getUsername() {
		String username;
		username=FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		return username;
	}
	
	public boolean isUserInRole(String... roles) {
		boolean inRole=false;
		for (String role : roles) {
			inRole=inRole || FacesContext.getCurrentInstance().getExternalContext().isUserInRole(role);
			if (inRole) break;
		}		
		return inRole;
	}
	
	public String logOff() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.invalidate();	
		return "logout";
	}
	
	public TorEvent getOpenTorEvent() {
		return serviceFactory.getTorEventService().getOpen();
	}
	
	public String getFirst() {		
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public ServiceFactory getServiceFactory() {
		return serviceFactory;
	}

	public void setServiceFactory(ServiceFactory serviceFactory) {
		this.serviceFactory = serviceFactory;
	}

	public TorCountry getTorCountry() {
		return torCountry;
	}

	public void setTorCountry(TorCountry torCountry) {
		this.torCountry = torCountry;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMainView() {
		return mainView;
	}

	public void setMainView(String mainView) {
		this.mainView = mainView;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

}
