package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tockacrta.trailOer.model.TorClub;

public class TorClubEdit extends BaseEdit<TorClub> implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -830083775822947847L;
	
	@Override
	public void setClazz() {
		clazz=TorClub.class;		
	}

    @PostConstruct
    public void init() {
    	example=new TorClub();
    	dataList = new LazyDataModel<TorClub>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
		    public List<TorClub> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				currentPage=serviceFactory.getTorClubService().getByExample(example, first, pageSize, sorts);
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorClubService().getByExampleSize(example));
    }
    
    public String search() {
    	dataList.setRowCount(serviceFactory.getTorClubService().getByExampleSize(example));
    	return null;
    }
        
    public String deleteRow() {
    	serviceFactory.getTorClubService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		serviceFactory.getTorClubService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		throw ex;
    	}
    	return null;
    }	 
	
}
