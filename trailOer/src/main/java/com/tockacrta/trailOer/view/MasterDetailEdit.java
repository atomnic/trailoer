package com.tockacrta.trailOer.view;


public abstract class MasterDetailEdit<M, T> extends BaseEdit<M> {
	

	protected BaseEdit<T> detail;
	
	public MasterDetailEdit() {
		super();
	}

	public MasterDetailEdit(BaseEdit<T> detail) {
		super();
		this.detail=detail;
		
	}
		
	public BaseEdit<T> getDetail() {
		return detail;
	}

	public void setDetail(BaseEdit<T> detail) {
		this.detail = detail;
	}
	
}
