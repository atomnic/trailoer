package com.tockacrta.trailOer.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.FacesEvent;

import org.hibernate.engine.transaction.jta.platform.internal.JOTMJtaPlatform;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.SortOrder;
import org.primefaces.model.LazyDataModel;

import java.util.Map;

import com.tockacrta.trailOer.model.TorCmrCmnRun;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.model.TorSolution;
import com.tockacrta.trailOer.model.TorTask;
import com.tockacrta.trailOer.service.ranking.model.PreOData;
import com.tockacrta.trailOer.view.util.UIComponentUtils;

public class TorSolutionEdit extends BaseEdit<TorSolution> implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6474144878728471065L;

	protected TorCmrCmnRunEdit master; 
	private Boolean hotKeyPressed = false;
	private Boolean control;
	private int startMin;	
	private int finishMin;
	private int startSec;
	private int finishSec;
	private PreOData info;
	
	public void setClazz() {
		this.clazz=TorSolution.class;
	}
	
	public Boolean masterize() {
		if (master!=null) { 
			if (master.getSelectedRow()!=null) {
				example.setTorCmrCmnRun(master.getSelectedRow());
			} else {			
				return false;
			}
		}	
		return true;
	}
	
	public String preliminary() {
		info=serviceFactory.getPreORankingService().preliminaryResults(master.getSelectedRow());
		return null;
	}
	
    @PostConstruct
    public void init() {
    		example=new TorSolution();
    		dataList= new LazyDataModel<TorSolution>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;		
			
			@Override
		    public List<TorSolution> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {				
				// if (hotKeyPressed) return currentPage;  //  load it just once when comopetitro ischanged
				HashMap<String, Boolean> sorts=new HashMap<String, Boolean>();
				if (sortOrder.compareTo(SortOrder.ASCENDING)>0) {
					sorts.put(sortField, true);
				}
				sorts.put("tsk.fullNo", true);
				if (masterize()) {
					dataList.setRowCount(serviceFactory.getTorSolutionService().getByExampleSize(example, true)); 
					if (dataList.getRowCount()==0) {
						serviceFactory.getTorSolutionService().generate(master.getSelectedRow());
					}
					// sorts.put("typType.code", true);
					// sorts.put("tsk.no", true);
					currentPage=serviceFactory.getTorSolutionService().getByExample(example,true, first, pageSize, sorts);
					if (currentPage==null || currentPage.size()==0) {
						// generate
						// TODO delete this if, if if above works :)
					}
				} else {
					if (currentPage!=null) {
						currentPage.clear();
					} else {
						currentPage=new ArrayList<TorSolution>();
					}
				}
				return currentPage;
			};										
    	};
    	// ovo moraš raditi kod svakoe promjene filtera!?
    	dataList.setRowCount(serviceFactory.getTorSolutionService().getByExampleSize(example, true)); 	
    }
    
    public void naviListener(ActionEvent event) {
    	UIComponent cb= UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
    	String forTable=(String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("forTable"); 
    	Integer ud=Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("step"));
    	UIComponent dt=cb.getParent().findComponent(forTable);    	
		int i=currentPage.indexOf(selectedRow);
		i+=ud;
		if (i>currentPage.size()-1) {
			// dataList.getNextPage			
			i=0; //(temporary - UHO NA TASK						
		} else if (i<0) {
			// dataList.getPrevPage
			i=currentPage.size()-1;
		}
		selectedRow=currentPage.get(i);
		((DataTable) dt).setSelection(selectedRow);
    }

    public void hotKeyListenerInternal(ActionEvent event) {
    	UIComponent cb= UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
    	String forTable=(String)UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("forTable");
    	String keyFromListener=(String)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("key");
    	int keyInt=Integer.parseInt(keyFromListener);
    	String key=Character.toString ((char) keyInt);
    	if (key.matches("[a-fA-F[xXzZ]]")) {
        	hotKeyPressed=true;
        	if (selectedRow==null) {
        		selectedRow=currentPage.get(0);    		
        	} 
	    	UIComponent dt=cb.getParent().findComponent(forTable);
	    	TorSolution torSolution=selectedRow;
	    	String solution=key;
	    	torSolution.setSolution(solution);
	    	serviceFactory.getTorSolutionService().save(torSolution);    	
			int i=currentPage.indexOf(selectedRow);
			if (i<currentPage.size()-1) { 
				selectedRow=currentPage.get(i+1);
			} else {
				// dataList.getNextPage			
				i=0; //(temporary - UHO NA TASK
			}
			((DataTable) dt).setSelection(selectedRow);
    	}
    }
    
    public String saveTimes() {
    	TorCmrCmnRun ccr= master.getSelectedRow();
    	long startMili=this.startMin*60*1000+this.startSec*1000;
    	long finishMili=this.finishMin*60*1000+this.finishSec*1000;
    	Date start= new Date(ccr.getTorRun().getStartTime().getTime()+startMili);
    	Date finish= new Date(ccr.getTorRun().getStartTime().getTime()+finishMili);
    	ccr.setActualStart(start);
    	ccr.setFinishTime(finish);
    	serviceFactory.getTorCmrCmnRunService().save(ccr);
    	//
    	return null;
    }
        
    public String hotKey() {
    	// hotKeyListenerInternal(event, "A");
    	return null;
    }
    
    public void hotKeyListener(ActionEvent event) {
    	hotKeyListenerInternal(event);
    }
        
    
    
    public String search() {
    	if (masterize()) {
    		dataList.setRowCount(serviceFactory.getTorSolutionService().getByExampleSize(example, true));
    	} else {
    		dataList.setRowCount(0);
    	}
    	return null;
    }
    
    
    @Override
    public String createRow() {
    	if (master!=null && master.getSelectedRow()!=null) {
        	super.createRow();
    		newRow.setTorCmrCmnRun(master.getSelectedRow());
    	} else if (master.getSelectedRow()==null) {
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("First choose stage/run!", null);
    	}
    	return null;
    };
    
    public String deleteRow() {
    	serviceFactory.getTorSolutionService().delete(selectedRow);
    	return null;
    }

    public String save() {
    	try {
    		if (master!=null) {
    			master.save();
    		}
    		serviceFactory.getTorSolutionService().saveAll(changedRows);
    		changed=false;
    		changedRows.clear();
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    public String saveRow() {
    	try {
    		serviceFactory.getTorSolutionService().save(selectedRow); 
    	} catch (Exception ex) {
    		// throw ex;
    		FacesContext.getCurrentInstance().validationFailed();
    		addError("Pospremanje neuspješno", ex);
    		System.out.println(ex.getMessage());
    		ex.printStackTrace();
    	}
    	return null;
    }
    
    public String resultAction() {
    	CommandButton cb=(CommandButton) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
    	DataTable dt=UIComponentUtils.findFirstDatTableParent(cb);
    	Long sltId=(Long) dt.getRowKey();
    	TorSolution torSolution=(TorSolution) dt.getRowData();
    	String solution=(String) cb.getValue();
    	torSolution.setSolution(solution);
    	serviceFactory.getTorSolutionService().save(torSolution);
    	return null;
    }

	public TorCmrCmnRunEdit getMaster() {
		return master;
	}

	public void setMaster(TorCmrCmnRunEdit master) {
		this.master = master;		
	}

	public Boolean getHotKeyPressed() {
		return hotKeyPressed;
	}

	public void setHotKeyPressed(Boolean hotKeyPressed) {
		this.hotKeyPressed = hotKeyPressed;
	}

	public Boolean getControl() {
		return control;
	}

	public void setControl(Boolean control) {
		this.control = control;
	}

	public int getStartMin() {
		if (master.getSelectedRow()!=null) {
			if (master.getSelectedRow().getActualStart()!=null) {
				long diff=master.getSelectedRow().getActualStart().getTime()-master.getSelectedRow().getTorRun().getStartTime().getTime();
				return (int) Math.floor(diff/60000F);
			}
		}
		return startMin;
	}

	public int getFinishMin() {
		if (master.getSelectedRow()!=null) {
			if (master.getSelectedRow().getFinishTime()!=null) {
				long diff=master.getSelectedRow().getFinishTime().getTime()-master.getSelectedRow().getTorRun().getStartTime().getTime();
				return (int) Math.floor(diff/60000F);
			}
		}		
		return finishMin;
	}

	public int getStartSec() {
		if (master.getSelectedRow()!=null) {
			if (master.getSelectedRow().getActualStart()!=null) {
				long diff=master.getSelectedRow().getActualStart().getTime()-master.getSelectedRow().getTorRun().getStartTime().getTime();
				diff=diff-getStartMin()*60000;
				return (int) Math.floor(diff/1000F);
			}
		}		
		return startSec;
	}

	public int getFinishSec() {
		if (master.getSelectedRow()!=null) {
			if (master.getSelectedRow().getActualStart()!=null) {
				long diff=master.getSelectedRow().getFinishTime().getTime()-master.getSelectedRow().getTorRun().getStartTime().getTime();
				diff=diff-getFinishMin()*60000;
				return (int) Math.floor(diff/1000F);
			}
		}			
		return finishSec;
	}

	public void setStartMin(int startMin) {
		this.startMin = startMin;
	}

	public void setFinishMin(int finishMin) {
		this.finishMin = finishMin;
	}

	public void setStartSec(int startSec) {
		this.startSec = startSec;
	}

	public void setFinishSec(int finishSec) {
		this.finishSec = finishSec;
	}

	public PreOData getInfo() {
		return info;
	}

	public void setInfo(PreOData info) {
		this.info = info;
	}
	 
}
