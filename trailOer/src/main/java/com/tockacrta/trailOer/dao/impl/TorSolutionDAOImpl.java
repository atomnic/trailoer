package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorSolutionDAO;
import com.tockacrta.trailOer.model.TorSolution;

public class TorSolutionDAOImpl  extends BaseHibernateDAOImpl<TorSolution, Long> implements TorSolutionDAO {

	@Override
	void setClazz() {
		super.clazz = TorSolution.class;
	}

	@Override
	public int findByExampleSize(TorSolution example, boolean solutionEntry) {
		Criteria criteria = getCurrentSession().createCriteria(TorSolution.class, "slt")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		criteria=criteria.createCriteria("slt.torTask", "tsk");
		criteria=criteria.createCriteria("tsk.torTypeType", "typType");
//		if (solutionEntry) {
//			criteria=criteria.add(Restrictions.ne("typType.code", "st"));
//		}
		if (example.getTorCmrCmnRun()!=null && example.getTorCmrCmnRun().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("slt.torCmrCmnRun", example.getTorCmrCmnRun()));
		}
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorSolution> findByExample(TorSolution example, boolean solutionEntry, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorSolution.class, "slt")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		criteria=criteria.createCriteria("slt.torTask", "tsk");
		criteria=criteria.createCriteria("tsk.torTypeType", "typType");
//		if (solutionEntry) {
//			criteria=criteria.add(Restrictions.ne("typType.code", "st"));
//		}		
		if (example.getTorCmrCmnRun()!=null && example.getTorCmrCmnRun().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("slt.torCmrCmnRun", example.getTorCmrCmnRun()));
		}	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
