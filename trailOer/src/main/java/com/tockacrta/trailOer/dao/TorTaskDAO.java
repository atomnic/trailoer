package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorTask;

public interface TorTaskDAO extends BaseHibernateDAO<TorTask, Long>{
	
	int findByExampleSize(TorTask example);
	List<TorTask> findByExample(TorTask example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
