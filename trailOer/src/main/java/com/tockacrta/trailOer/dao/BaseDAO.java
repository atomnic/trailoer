package com.tockacrta.trailOer.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface BaseDAO<T, ID extends Serializable> {
	public T save(T entity);
	public T merge(T entity);
	public void saveAll(Collection<T> entities);
	public void delete(T entity);
	public T findById(ID id);
	public List<T> findAll();
	public List<T> findByExample(T example);
}
