package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCountry;

public interface TorCountryDAO extends BaseHibernateDAO<TorCountry, Long>{
	
	int findByExampleSize(TorCountry example);
	List<TorCountry> findByExample(TorCountry example, int firstElement, int maxElements, Map<String, Boolean> sorts);
}
