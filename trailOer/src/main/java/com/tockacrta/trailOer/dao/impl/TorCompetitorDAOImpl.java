package com.tockacrta.trailOer.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorCompetitorDAO;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.models.Auditing;

public class TorCompetitorDAOImpl  extends BaseHibernateDAOImpl<TorCompetitor, Long> implements TorCompetitorDAO {

	@Override
	void setClazz() {
		super.clazz = TorCompetitor.class;
	}

	@Override
	public int findByExampleSize(TorCompetitor example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCompetitor.class, "cmp")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorCountry()!=null) {
			if (example.getTorCountry().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torCountry.id", example.getTorCountry().getId()));
			}
		}
		if (example.getTorType()!=null) {
			if (example.getTorType().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torType.id", example.getTorType().getId()));
			}
		}
		if (example.getTorTypePara()!=null) {
			if (example.getTorTypePara().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torTypePara.id", example.getTorTypePara().getId()));
			}
		}
		if (example.getTorTypeSex()!=null) {
			if (example.getTorTypeSex().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torTypeSex.id", example.getTorTypeSex().getId()));
			}
		}
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCompetitor> findByExample(TorCompetitor example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCompetitor.class, "cmp")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorCountry()!=null) {
			if (example.getTorCountry().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torCountry.id", example.getTorCountry().getId()));
			}
		}	
		if (example.getTorType()!=null) {
			if (example.getTorType().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torType.id", example.getTorType().getId()));
			}
		}
		if (example.getTorTypePara()!=null) {
			if (example.getTorTypePara().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torTypePara.id", example.getTorTypePara().getId()));
			}
		}
		if (example.getTorTypeSex()!=null) {
			if (example.getTorTypeSex().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmp.torTypeSex.id", example.getTorTypeSex().getId()));
			}
		}		
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}
	
	@Override
	public TorCompetitor save(TorCompetitor entity) {
		if (entity instanceof Auditing) {
			Auditing entityA=(Auditing)entity;
			if (entityA.getDateCreated() ==null) {
				entityA.setDateCreated(new Date());
			}
		}		
		
		this.getCurrentSession().saveOrUpdate(entity);
		
		this.saveHistory(entity);
		
		return entity;
	}

}
