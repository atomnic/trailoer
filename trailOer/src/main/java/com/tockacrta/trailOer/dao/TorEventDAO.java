package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorEvent;

public interface TorEventDAO extends BaseHibernateDAO<TorEvent, Long>{
	
	int findByExampleSize(TorEvent example);
	List<TorEvent> findByExample(TorEvent example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	List<TorEvent> getOpen();
}
