package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCategory;

public interface TorCategoryDAO extends BaseHibernateDAO<TorCategory, Long>{
	
	int findByExampleSize(TorCategory example);
	List<TorCategory> findByExample(TorCategory example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
