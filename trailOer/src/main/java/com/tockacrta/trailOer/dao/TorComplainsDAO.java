package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorComplains;

public interface TorComplainsDAO extends BaseHibernateDAO<TorComplains, Long>{
	
	int findByExampleSize(TorComplains example);
	List<TorComplains> findByExample(TorComplains example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
