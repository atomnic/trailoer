package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;

import com.tockacrta.trailOer.dao.TorCategoryDAO;
import com.tockacrta.trailOer.model.TorCategory;

public class TorCategoryDAOImpl  extends BaseHibernateDAOImpl<TorCategory, Long> implements TorCategoryDAO {

	@Override
	void setClazz() {
		super.clazz = TorCategory.class;
	}

	@Override
	public int findByExampleSize(TorCategory example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCategory.class, "ctg")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());

		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCategory> findByExample(TorCategory example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCategory.class, "ctg")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
