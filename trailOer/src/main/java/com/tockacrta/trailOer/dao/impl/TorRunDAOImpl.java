package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorRunDAO;
import com.tockacrta.trailOer.model.TorRun;

public class TorRunDAOImpl  extends BaseHibernateDAOImpl<TorRun, Long> implements TorRunDAO {

	@Override
	void setClazz() {
		super.clazz = TorRun.class;
	}

	@Override
	public int findByExampleSize(TorRun example) {
		Criteria criteria = getCurrentSession().createCriteria(TorRun.class, "run")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorCompetition()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torCompetition.id", example.getTorCompetition().getId()));
			} else	if (example.getTorCompetition().getTorEvent()!=null && example.getTorCompetition().getTorEvent().getId()!=null) {
				criteria=criteria.createCriteria("run.torCompetition", "cmn")
								.add(Restrictions.eq("cmn.torEvent.id", example.getTorCompetition().getTorEvent().getId()));
			}
		}		
		if (example.getTorTypes()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torTypes.id", example.getTorTypes().getId()));
			}
		}	
		if (example.getTorTypeStatus()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torTypeStatus.id", example.getTorTypeStatus().getId()));
			}
		}	
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorRun> findByExample(TorRun example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorRun.class, "run")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorCompetition()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torCompetition.id", example.getTorCompetition().getId()));
			} else	if (example.getTorCompetition().getTorEvent()!=null && example.getTorCompetition().getTorEvent().getId()!=null) {
				criteria=criteria.createCriteria("run.torCompetition", "cmn")
								.add(Restrictions.eq("cmn.torEvent.id", example.getTorCompetition().getTorEvent().getId()));
			}
		}	
		if (example.getTorTypes()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torTypes.id", example.getTorTypes().getId()));
			}
		}		
		if (example.getTorTypeStatus()!=null) {
			if (example.getTorCompetition().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("run.torTypeStatus.id", example.getTorTypeStatus().getId()));
			}
		}			
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}
	

}
