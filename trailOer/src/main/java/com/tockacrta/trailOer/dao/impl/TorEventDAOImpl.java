package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorEventDAO;
import com.tockacrta.trailOer.model.TorEvent;

public class TorEventDAOImpl  extends BaseHibernateDAOImpl<TorEvent, Long> implements TorEventDAO {

	@Override
	void setClazz() {
		super.clazz = TorEvent.class;
	}

	@Override
	public int findByExampleSize(TorEvent example) {
		Criteria criteria = getCurrentSession().createCriteria(TorEvent.class, "evn")				
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorEvent> findByExample(TorEvent example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorEvent.class, "evn")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

	@Override
	public List<TorEvent> getOpen() {
		Criteria criteria = getCurrentSession().createCriteria(TorEvent.class, "evn")
				.createCriteria("evn.torTypeStatus","typ");
		criteria=criteria.add(Restrictions.eq("typ.code", "open"));
		List<TorEvent> result=criteria.list();
		return result;
	}

}
