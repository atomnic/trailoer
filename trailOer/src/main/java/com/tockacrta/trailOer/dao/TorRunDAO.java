package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorRun;

public interface TorRunDAO extends BaseHibernateDAO<TorRun, Long>{
	
	int findByExampleSize(TorRun example);
	List<TorRun> findByExample(TorRun example, int firstElement, int maxElements, Map<String, Boolean> sorts);	
}
