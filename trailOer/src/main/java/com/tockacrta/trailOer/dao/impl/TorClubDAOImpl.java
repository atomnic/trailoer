package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;

import com.tockacrta.trailOer.dao.TorClubDAO;
import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorClub;

public class TorClubDAOImpl  extends BaseHibernateDAOImpl<TorClub, Long> implements TorClubDAO {

	@Override
	void setClazz() {
		super.clazz = TorClub.class;
	}
	
	public int findByExampleSize(TorClub example) {
		Criteria criteria = getCurrentSession().createCriteria(TorClub.class, "ctg")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());

		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorClub> findByExample(TorClub example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorClub.class, "ctg")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
