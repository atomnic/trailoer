package com.tockacrta.trailOer.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

public interface BaseHibernateDAO<T, ID extends Serializable> extends BaseDAO<T, ID> {
	public List<T> findByCriteria(final Criterion... criterions);
	public T findUniqueByCriteria(final Criterion... criterions);
	public void lockLazyObject(Object object);
	public Boolean getCacheable();
	public void setCacheable(Boolean cacheable);
	//public Session getCurrentSession();
}
