package com.tockacrta.trailOer.dao.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import com.tockacrta.trailOer.dao.TorCompetitionDAO;
import com.tockacrta.trailOer.model.TohCompetition;
import com.tockacrta.trailOer.model.TohCompetitor;
import com.tockacrta.trailOer.model.TohCountry;
import com.tockacrta.trailOer.model.TohEvent;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.models.Auditing;

public class TorCompetitionDAOImpl  extends BaseHibernateDAOImpl<TorCompetition, Long> implements TorCompetitionDAO {

	@Override
	void setClazz() {
		super.clazz = TorCompetition.class;
	}

	@Override
	public int findByExampleSize(TorCompetition example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCompetition.class, "cmn")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorEvent()!=null) {
			if (example.getTorEvent().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmn.torEvent.id", example.getTorEvent().getId()));
			}
		}
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCompetition> findByExample(TorCompetition example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCompetition.class, "cmn")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorEvent()!=null) {
			if (example.getTorEvent().getId()!=null) {
				criteria=criteria.add(Restrictions.eq("cmn.torEvent.id", example.getTorEvent().getId()));
			}
		}		
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

	@Override
	public List<TorCompetition> findByEvent(TorEvent evn) {
		if (evn==null ) { 
			return null;
		}
		Criteria criteria = getCurrentSession().createCriteria(TorCompetition.class, "cmn");
		criteria=criteria.add(Restrictions.eq("cmn.torEvent.id", evn.getId()));
		
		List<TorCompetition> result=criteria.list();
		return result;
	}
	
	@Override
	public List<TorCompetition> findByCompetitor(TorCompetitor cmr) {
		if ( cmr==null ) { 
			return null;
		}
		Criteria criteria = getCurrentSession().createCriteria(TorCompetition.class, "cmn");
		criteria=criteria.add(Restrictions.eq("cmn.torCompetitor.id", cmr.getId()));

		List<TorCompetition> result=criteria.list();
		return result;
	}

	@Override
	public List<TorCompetition> findByEvnAndCmr(TorEvent evn,
			TorCompetitor cmr, Boolean selected) {
		Criteria criteria = getCurrentSession().createCriteria(TorCompetition.class, "cmn")
				.add(Restrictions.eq("cmn.torEvent", evn));
		
		DetachedCriteria dc=DetachedCriteria.forClass(TorCmrCmn.class,"cmm")
				.add(Restrictions.eq("cmm.torCompetitor", cmr))
				.setProjection(Projections.property("cmm.torCompetition.id"));
		
		if (selected) {
			criteria=criteria.add(Subqueries.propertyIn("cmn.id", dc));
		} else {
			criteria=criteria.add(Subqueries.propertyNotIn("cmn.id", dc));
		}
		
		List<TorCompetition> result=criteria.list();
	return result;
	}
	
	/*
	@Override
	public TorCompetition save(TorCompetition entity) {
		if (entity instanceof Auditing) {
			Auditing entityA=(Auditing)entity;
			if (entityA.getDateCreated() ==null) {
				entityA.setDateCreated(new Date());
			}
		}		
		
		this.getCurrentSession().saveOrUpdate(entity);
		
		Class<?> clazzH;
		try {			
			clazzH = Class.forName(entity.getClass().getName().replace("Tor", "Toh"));			
			// da se cachira			
			Constructor<?> ctor = clazzH.getConstructor(clazz, String.class);			
			
			TohCompetition example = (TohCompetition) ctor.newInstance(new Object[] { entity, null });
			example.setTohEvent(new TohEvent());
			example.getTohEvent().setIdm(entity.getTorEvent().getId());
			// Method method=clazzH.getMethod("setIdm", Long.class);
			// method.invoke(example, getCurrentSession().getIdentifier(entity));
			Criteria criteria=this.getCurrentSession().createCriteria(clazzH, "cmr").add(Example.create(example).enableLike(MatchMode.EXACT)
					.excludeZeroes());
			criteria=criteria.createCriteria("tohCountry").add(Example.create(example.getTohEvent()).enableLike(MatchMode.EXACT).excludeZeroes());			
			List<?> result= criteria.list();
			
			
			if (result==null || result.size()==0) {
				TohCompetition object = (TohCompetition) ctor.newInstance(new Object[] { entity, "U" });
				Criteria criteria2=this.getCurrentSession()
							.createCriteria(TohCountry.class, "cny")
							.add(Restrictions.eq("idm", entity.getTorEvent().getId()))
							.addOrder(Order.desc("id"))
							.setFirstResult(0)
							.setMaxResults(1);
				object.setTohEvent((TohEvent)criteria2.list().get(0));
				this.getCurrentSession().saveOrUpdate(object);
			}
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return entity;
	}
	*/

}
