package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorTypes;

public interface TorTypesDAO extends BaseHibernateDAO<TorTypes, Long>{
	
	int findByExampleSize(TorTypes example);
	List<TorTypes> findByExample(TorTypes example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	List<TorTypes> findByDomain(String domain, String code);
	
}
