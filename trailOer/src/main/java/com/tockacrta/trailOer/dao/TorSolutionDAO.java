package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorSolution;

public interface TorSolutionDAO extends BaseHibernateDAO<TorSolution, Long>{
	
	int findByExampleSize(TorSolution example, boolean solutionEntry);
	List<TorSolution> findByExample(TorSolution example, boolean solutionEntry, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
