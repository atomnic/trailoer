package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorTaskDAO;
import com.tockacrta.trailOer.model.TorTask;

public class TorTaskDAOImpl  extends BaseHibernateDAOImpl<TorTask, Long> implements TorTaskDAO {

	@Override
	void setClazz() {
		super.clazz = TorTask.class;
	}

	@Override
	public int findByExampleSize(TorTask example) {
		Criteria criteria = getCurrentSession().createCriteria(TorTask.class, "tsk")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorRun()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torRun",example.getTorRun()));
		}
		if (example.getTorTypeType()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torTypeType",example.getTorTypeType()));
		}	
		if (example.getTorTypeStatus()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torTypeStatus",example.getTorTypeStatus()));
		}
		if (example.getTorCategory()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torCategory",example.getTorCategory()));
		}
		
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorTask> findByExample(TorTask example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorTask.class, "tsk")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorRun()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torRun",example.getTorRun()));
		}
		if (example.getTorTypeType()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torTypeType",example.getTorTypeType()));
		}
		if (example.getTorTypeStatus()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torTypeStatus",example.getTorTypeStatus()));
		}
		if (example.getTorCategory()!=null) {
			criteria=criteria.add(Restrictions.eq("tsk.torCategory",example.getTorCategory()));
		}
		
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
