package com.tockacrta.trailOer.dao.impl;

import com.tockacrta.trailOer.dao.DaoFactory;
import com.tockacrta.trailOer.dao.TorCategoryDAO;
import com.tockacrta.trailOer.dao.TorClubDAO;
import com.tockacrta.trailOer.dao.TorCmrCmnDAO;
import com.tockacrta.trailOer.dao.TorCmrCmnRunDAO;
import com.tockacrta.trailOer.dao.TorCompetitionDAO;
import com.tockacrta.trailOer.dao.TorCompetitorDAO;
import com.tockacrta.trailOer.dao.TorComplainsDAO;
import com.tockacrta.trailOer.dao.TorCountryDAO;
import com.tockacrta.trailOer.dao.TorEventDAO;
import com.tockacrta.trailOer.dao.TorRunDAO;
import com.tockacrta.trailOer.dao.TorSolutionDAO;
import com.tockacrta.trailOer.dao.TorTaskDAO;
import com.tockacrta.trailOer.dao.TorTypesDAO;
import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCmrCmnRun;

public class DaoFactoryImpl implements DaoFactory{

	private TorClubDAO torClubDAO;
	private TorCountryDAO torCountryDAO;
	private TorCompetitorDAO torCompetitorDAO;
	private TorEventDAO torEventDAO;
	
	private TorCategoryDAO torCategoryDAO;
	private TorCmrCmnDAO torCmrCmnDAO;

	private TorCmrCmnRunDAO torCmrCmnRunDAO;
	
	private TorCompetitionDAO torCompetitionDAO;
	
	private TorComplainsDAO torComplainsDAO;
	
	private TorRunDAO torRunDAO;
	
	private TorSolutionDAO torSolutionDAO;
	
	private TorTaskDAO torTaskDAO;
	
	private TorTypesDAO torTypesDAO;	
	
	public void setTorClubDAO(TorClubDAO torClubDAO) {
		this.torClubDAO=torClubDAO;
	}
	
	@Override
	public TorClubDAO getTorClubDAO() {
		return torClubDAO;
	}
	
	public void setTorCountryDAO(TorCountryDAO torCountryDAO) {
		this.torCountryDAO=torCountryDAO;
	}
	
	@Override
	public TorCountryDAO getTorCountryDAO() {
		return torCountryDAO;
	}
	
	@Override
	public TorCompetitorDAO getTorCompetitorDAO() {
		return torCompetitorDAO;
	}

	public void setTorCompetitorDAO(TorCompetitorDAO torCompetitorDAO) {
		this.torCompetitorDAO = torCompetitorDAO;
	}

	public TorEventDAO getTorEventDAO() {
		return torEventDAO;
	}

	public void setTorEventDAO(TorEventDAO torEventDAO) {
		this.torEventDAO = torEventDAO;
	}

	public TorCompetitionDAO getTorCompetitionDAO() {
		return torCompetitionDAO;
	}

	public void setTorCompetitionDAO(TorCompetitionDAO torCompetitionDAO) {
		this.torCompetitionDAO = torCompetitionDAO;
	}

	public TorCategoryDAO getTorCategoryDAO() {
		return torCategoryDAO;
	}

	public void setTorCategoryDAO(TorCategoryDAO torCategoryDAO) {
		this.torCategoryDAO = torCategoryDAO;
	}

	public TorCmrCmnDAO getTorCmrCmnDAO() {
		return torCmrCmnDAO;
	}

	public void setTorCmrCmnDAO(TorCmrCmnDAO torCmrCmnDAO) {
		this.torCmrCmnDAO = torCmrCmnDAO;
	}

	public TorCmrCmnRunDAO getTorCmrCmnRunDAO() {
		return torCmrCmnRunDAO;
	}

	public void setTorCmrCmnRunDAO(TorCmrCmnRunDAO torCmrCmnRunDAO) {
		this.torCmrCmnRunDAO = torCmrCmnRunDAO;
	}

	public TorComplainsDAO getTorComplainsDAO() {
		return torComplainsDAO;
	}

	public void setTorComplainsDAO(TorComplainsDAO torComplainsDAO) {
		this.torComplainsDAO = torComplainsDAO;
	}

	public TorRunDAO getTorRunDAO() {
		return torRunDAO;
	}

	public void setTorRunDAO(TorRunDAO torRunDAO) {
		this.torRunDAO = torRunDAO;
	}

	public TorSolutionDAO getTorSolutionDAO() {
		return torSolutionDAO;
	}

	public void setTorSolutionDAO(TorSolutionDAO torSolutionDAO) {
		this.torSolutionDAO = torSolutionDAO;
	}

	public TorTaskDAO getTorTaskDAO() {
		return torTaskDAO;
	}

	public void setTorTaskDAO(TorTaskDAO torTaskDAO) {
		this.torTaskDAO = torTaskDAO;
	}

	public TorTypesDAO getTorTypesDAO() {
		return torTypesDAO;
	}

	public void setTorTypesDAO(TorTypesDAO torTypesDAO) {
		this.torTypesDAO = torTypesDAO;
	}


	
}
