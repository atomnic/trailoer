package com.tockacrta.trailOer.dao;

public interface DaoFactory {
	TorClubDAO getTorClubDAO();
	TorCountryDAO getTorCountryDAO();
	TorCompetitorDAO getTorCompetitorDAO();
	TorEventDAO getTorEventDAO();
	TorCompetitionDAO getTorCompetitionDAO();

	TorCategoryDAO getTorCategoryDAO();
	TorCmrCmnDAO getTorCmrCmnDAO();
	TorCmrCmnRunDAO getTorCmrCmnRunDAO();
	TorComplainsDAO getTorComplainsDAO();
	TorRunDAO getTorRunDAO();

	TorSolutionDAO getTorSolutionDAO();
	TorTaskDAO getTorTaskDAO();
	TorTypesDAO getTorTypesDAO();
	
}
