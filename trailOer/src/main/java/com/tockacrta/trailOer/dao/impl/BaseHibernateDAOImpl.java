package com.tockacrta.trailOer.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.tockacrta.trailOer.dao.BaseHibernateDAO;
import com.tockacrta.trailOer.model.TohCompetitor;
import com.tockacrta.trailOer.model.TohCountry;
import com.tockacrta.trailOer.model.TorRun;
import com.tockacrta.trailOer.models.Auditing;

public abstract class BaseHibernateDAOImpl<T, ID extends Serializable> implements BaseHibernateDAO<T, ID> {
	protected Class<T> clazz;
	protected Class<?> clazzH;

	protected SessionFactory sessionFactory;
	
	private Boolean cacheable=true;	

	public BaseHibernateDAOImpl() {
		setClazz();
		try {
			clazzH = Class.forName(clazz.getName().replace("Tor", "Toh"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public Boolean getCacheable() {
		return cacheable;
	}

	public void setCacheable(Boolean cacheable) {
		this.cacheable = cacheable;
	}
	
	protected Criteria addSort(Criteria criteria, Map<String, Boolean> sorts) {
		if (sorts!=null && sorts.size()>0 ) {
			for (Entry<String, Boolean> sort : sorts.entrySet()) {						
				if (sort.getValue()!=null && sort.getValue()) {
					criteria=criteria.addOrder(Order.asc(sort.getKey()));
				} else {
					criteria=criteria.addOrder(Order.desc(sort.getKey()));
				}	
			}
		}
		return criteria;
	}
	
	abstract void setClazz();

	public void delete(T entity) {
		sessionFactory.getCurrentSession().delete(entity);
		// getHibernateTemplate().delete(entity);
	}

	public List<T> findAll() {
		return findByCriteria();
	}

	public List<T> findByExample(T example, int start, int size, Map<String, Boolean> sorts) {
		return findByCriteria(0, 100, Example.create(example));
	}
	
	public List<T> findByExample(T example) {
		return findByExample(example, 0, 0, null);
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return (T) this.getCurrentSession().get( this.clazz, id );
	}

	@SuppressWarnings("unchecked")
	public T merge(T entity) {
		return (T) this.getCurrentSession().merge(entity);
	}
	
	public Object historyConstructor(T entity, String cudm) {
		
		try {
			Constructor<?> ctor = clazzH.getConstructor();	
			Object pseudoThis=ctor.newInstance();
			
			if (cudm!=null) {
				clazzH.getMethod("setCudm", String.class).invoke(pseudoThis, cudm);
			}
			clazzH.getMethod("setIdm", Long.class).invoke(pseudoThis, (Long) entity.getClass().getMethod("getId").invoke(entity));
			Field[] fields=entity.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ((
						field.getType().getPackage()==null ||	// TODO primitives, find better solution
						field.getType().getPackage().getName().equals("java.lang") ||		// TODO riješiti bolje
						field.getType().getName().equals("java.util.Date")
					)
						&& !field.getName().equals("id")) 
				{ 
					//this.getClass().getDeclaredField(field.getName()).set(this, field.get(entity));
					Object value=clazz.getMethod("get"+StringUtils.capitalize(field.getName())).invoke(entity);
					clazzH.getMethod("set"+StringUtils.capitalize(field.getName()),field.getType()).invoke(pseudoThis, value);
				}
			}			
			return pseudoThis;
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveHistory(T entity) {
		try {					
			// da se cachira			
			///Constructor<?> ctor = clazzH.getConstructor(clazz, String.class);			
			
			///TohCompetitor example = (TohCompetitor) ctor.newInstance(new Object[] { entity, null });
			///Object example2=ctor.newInstance(new Object[] { entity, null });
			Object example2=historyConstructor(entity, null);
			Criteria criteria=this.getCurrentSession().createCriteria(clazzH, "cmr").add(Example.create(example2).enableLike(MatchMode.EXACT)
					.excludeZeroes());
			
			Method[] methods=clazzH.getMethods();
			for (Method method : methods) {
				Class<?>[] params=method.getParameterTypes();
				if (params.length==1 && method.getName().startsWith("setToh")) {
					if (params[0].getPackage().getName().equals("com.tockacrta.trailOer.model")) {
						Object obj1=params[0].getConstructor().newInstance();
						Method setIdm=params[0].getMethod("setIdm", Long.class);
						String method2Name=method.getName().replace("setToh", "getTor");
						Method getFk=clazz.getMethod(method2Name); //, Class.forName("com.tockacrta.trailOer.model."+method2Name.replace("get", "")));
						Object fkObj=getFk.invoke(entity);
						if (fkObj!=null) {
							Long fkId=(Long) fkObj.getClass().getMethod("getId").invoke(fkObj);
							setIdm.invoke(obj1, fkId);

							criteria=criteria.createCriteria("cmr."+StringUtils.uncapitalize(method.getName().replace("set", "")))
									.add(Example.create(obj1).enableLike(MatchMode.EXACT).excludeZeroes());
						}
					}
				}
			}
			
			List<?> result= criteria.list();
			
			// zanči ako nema povjesti 
			if (result==null || result.size()==0) {
				Object object =  historyConstructor(entity, "U"); ///ctor.newInstance(new Object[] { entity, "U" });
				Boolean skip=false;
				for (Method method : methods) {
					Class<?>[] params=method.getParameterTypes();
					if (params.length==1 && method.getName().startsWith("setToh")) {
						if (params[0].getPackage().getName().equals("com.tockacrta.trailOer.model")) {
							String methodNameR=method.getName().replace("setToh", "getTor");
							Method methodR=clazz.getMethod(methodNameR);
							Object tor= methodR.invoke(entity);
							if (tor!=null) {
								Long id=(Long) tor.getClass().getMethod("getId").invoke(tor);
								
								String className=params[0].getName();       // method.getName().replace("set", "");
								String alias=params[0].getSimpleName();
								
								Criteria criteria3=this.getCurrentSession()
										.createCriteria(Class.forName(className),alias)
										.add(Restrictions.eq("idm", id))
										.addOrder(Order.desc("id"))
										.setFirstResult(0)
										.setMaxResults(1);
								if (criteria3.list().size()>0) { // else no history, skip
									method.invoke(object, criteria3.list().get(0));
									skip=true;
								}
							}
						}
					}
				}
				if (!skip) {
					try {
						this.getCurrentSession().saveOrUpdate(object);
					} catch (Exception ex) {				
						// TODO ignored 
					}
				}
			}
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {				
			// TODO ignored 
		}
	}
	
	@SuppressWarnings("unchecked")
	public T save(T entity) {
		
		// TODO this is to be copied in preInsert/update
		if (entity instanceof Auditing) {
			Auditing entityA=(Auditing)entity;
			if (entityA.getDateCreated() ==null) {
				entityA.setDateCreated(new Date());
			}
		}	
		
		try {
			clazz.getMethod("setDateCreated", java.util.Date.class).invoke(entity, new Date());
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//
		this.getCurrentSession().saveOrUpdate(entity);
		
		// TODO this must go in postInsert/Update
		// this.saveHistory(entity);

		return entity;
	}

	public void saveAll(Collection<T> entities) {
		//getHibernateTemplate().saveOrUpdateAll(entities);
		this.getCurrentSession().save(entities);
	}

	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(int firstElement,
			int maxElements,final Criterion... criterions) {
		Criteria criteria = getCurrentSession().createCriteria(clazz);
		for (Criterion criterion : criterions) {
			criteria.add(criterion);
		}
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(final Criterion... criterions) {
		Criteria criteria = getCurrentSession().createCriteria(clazz);
		for (Criterion criterion : criterions) {
			criteria.add(criterion);
		}
	
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public T findUniqueByCriteria(final Criterion... criterions) {
		Criteria criteria = getCurrentSession().createCriteria(clazz);
		for (Criterion criterion : criterions) {
			criteria.add(criterion);
		}
		return (T) criteria.uniqueResult();
	}
	
	public void lockLazyObject(Object object) {
		// TODO ovo nam ne treba
		// koristiom za registraciju objekta sa sesisjom
		Session session= sessionFactory.getCurrentSession();
		session.lock(object, LockMode.NONE);
	}
	
	public void openSession() {
	  this.sessionFactory.openSession();
	}
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	protected final Session getCurrentSession(){
		return this.sessionFactory.getCurrentSession();
	}	
}
