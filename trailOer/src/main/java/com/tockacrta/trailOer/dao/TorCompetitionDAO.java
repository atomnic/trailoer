package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;
import com.tockacrta.trailOer.model.TorRun;

public interface TorCompetitionDAO extends BaseHibernateDAO<TorCompetition, Long>{
	
	int findByExampleSize(TorCompetition example);
	List<TorCompetition> findByExample(TorCompetition example, int firstElement, int maxElements, Map<String, Boolean> sorts);

	public List<TorCompetition> findByEvent(TorEvent evn);
	public List<TorCompetition> findByCompetitor(TorCompetitor cmr);
	public List<TorCompetition> findByEvnAndCmr(TorEvent evn, TorCompetitor cmr, Boolean selected);
}
