package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCompetitor;

public interface TorCompetitorDAO extends BaseHibernateDAO<TorCompetitor, Long>{
	
	int findByExampleSize(TorCompetitor example);
	List<TorCompetitor> findByExample(TorCompetitor example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
