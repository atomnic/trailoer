package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCompetition;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;

public interface TorCmrCmnDAO extends BaseHibernateDAO<TorCmrCmn, Long>{
	
	int findByExampleSize(TorCmrCmn example);
	List<TorCmrCmn> findByExample(TorCmrCmn example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	List<TorCmrCmn> findByCmrAndEvn(TorCompetitor cmr, TorEvent evn);

}
