package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorTypesDAO;
import com.tockacrta.trailOer.model.TorTypes;

public class TorTypesDAOImpl  extends BaseHibernateDAOImpl<TorTypes, Long> implements TorTypesDAO {

	@Override
	void setClazz() {
		super.clazz = TorTypes.class;
	}

	@Override
	public int findByExampleSize(TorTypes example) {
		Criteria criteria = getCurrentSession().createCriteria(TorTypes.class, "typ")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());

		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorTypes> findByExample(TorTypes example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorTypes.class, "typ")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

	@Override
	public List<TorTypes> findByDomain(String domain, String code) {
		Criteria criteria=getCurrentSession().createCriteria(TorTypes.class);
		criteria=criteria.add(Restrictions.eq("domain", domain));
		if (code!=null) {
			criteria=criteria.add(Restrictions.eq("code", code).ignoreCase());
		}
		return criteria.list();
	}

}
