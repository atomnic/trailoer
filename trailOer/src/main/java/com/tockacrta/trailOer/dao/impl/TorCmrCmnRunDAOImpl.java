package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorCmrCmnRunDAO;
import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmnRun;


public class TorCmrCmnRunDAOImpl  extends BaseHibernateDAOImpl<TorCmrCmnRun, Long> implements TorCmrCmnRunDAO {

	@Override
	void setClazz() {
		super.clazz = TorCmrCmnRun.class;
	}

	@Override
	public int findByExampleSize(TorCmrCmnRun example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCmrCmnRun.class, "ccr")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorRun()!=null && example.getTorRun().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("ccr.torRun", example.getTorRun()));
		}
		if (example.getTorTypeStatus()!=null && example.getTorTypeStatus().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("ccr.torTypeStatus", example.getTorTypeStatus()));
		}		
		criteria=criteria.createAlias("ccr.torCmrCmn", "cmm");
	
		if (example.getTorCmrCmn()!=null) {
			if (example.getTorCmrCmn().getTorCompetitor()!=null) {
				criteria=criteria.createAlias("cmm.torCompetitor", "cmr");
				if (example.getTorCmrCmn().getTorCompetitor().getLastName()!=null) {
					criteria=criteria.add(Restrictions.ilike("cmr.lastName", example.getTorCmrCmn().getTorCompetitor().getLastName(), MatchMode.START));
				}
				if (example.getTorCmrCmn().getTorCompetitor().getFirstName()!=null) {
					criteria=criteria.add(Restrictions.ilike("cmr.firstName", example.getTorCmrCmn().getTorCompetitor().getFirstName(), MatchMode.START));
				}				
			}
		}
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCmrCmnRun> findByExample(TorCmrCmnRun example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		return findByExampleCategory(example, null, firstElement, maxElements, sorts);
	}

	@Override
	public List<TorCmrCmnRun> findByExampleCategory(TorCmrCmnRun example,
			TorCategory torCategory, int firstElement, int maxElements,
			Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCmrCmnRun.class, "ccr")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		if (example.getTorRun()!=null && example.getTorRun().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("ccr.torRun", example.getTorRun()));
		}
		if (example.getTorTypeStatus()!=null && example.getTorTypeStatus().getId()!=null) {
			criteria=criteria.add(Restrictions.eq("ccr.torTypeStatus", example.getTorTypeStatus()));
		}
		criteria=criteria.createAlias("ccr.torCmrCmn", "cmm");
		if (torCategory!=null) {			
			criteria=criteria.add(Restrictions.eq("cmm.torCategory", torCategory));
		}
		if (example.getTorCmrCmn()!=null) {
			if (example.getTorCmrCmn().getTorCompetitor()!=null) {
				criteria=criteria.createAlias("cmm.torCompetitor", "cmr");
				if (example.getTorCmrCmn().getTorCompetitor().getLastName()!=null) {
					criteria=criteria.add(Restrictions.like("cmr.lastName", example.getTorCmrCmn().getTorCompetitor().getLastName()));
				}
				if (example.getTorCmrCmn().getTorCompetitor().getFirstName()!=null) {
					criteria=criteria.add(Restrictions.like("cmr.firstName", example.getTorCmrCmn().getTorCompetitor().getFirstName()));
				}				
			}
		}
		
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}


}
