package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorCategory;
import com.tockacrta.trailOer.model.TorCmrCmnRun;

public interface TorCmrCmnRunDAO extends BaseHibernateDAO<TorCmrCmnRun, Long>{
	
	int findByExampleSize(TorCmrCmnRun example);
	List<TorCmrCmnRun> findByExample(TorCmrCmnRun example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	List<TorCmrCmnRun> findByExampleCategory(TorCmrCmnRun example, TorCategory torCategory, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
