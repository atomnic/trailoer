package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;

import com.tockacrta.trailOer.dao.TorCountryDAO;
import com.tockacrta.trailOer.model.TorCountry;

public class TorCountryDAOImpl  extends BaseHibernateDAOImpl<TorCountry, Long> implements TorCountryDAO {

	@Override
	void setClazz() {
		super.clazz = TorCountry.class;
	}

	@Override
	public int findByExampleSize(TorCountry example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCountry.class, "cny")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		
		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCountry> findByExample(TorCountry example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCountry.class, "cny")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
