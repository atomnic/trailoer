package com.tockacrta.trailOer.dao;

import java.util.List;
import java.util.Map;

import com.tockacrta.trailOer.model.TorClub;

public interface TorClubDAO extends BaseHibernateDAO<TorClub, Long>{
	int findByExampleSize(TorClub example);
	List<TorClub> findByExample(TorClub example, int firstElement, int maxElements, Map<String, Boolean> sorts);
	
}
