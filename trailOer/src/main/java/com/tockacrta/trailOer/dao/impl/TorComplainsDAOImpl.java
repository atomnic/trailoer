package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;

import com.tockacrta.trailOer.dao.TorComplainsDAO;
import com.tockacrta.trailOer.model.TorComplains;

public class TorComplainsDAOImpl  extends BaseHibernateDAOImpl<TorComplains, Long> implements TorComplainsDAO {

	@Override
	void setClazz() {
		super.clazz = TorComplains.class;
	}

	@Override
	public int findByExampleSize(TorComplains example) {
		Criteria criteria = getCurrentSession().createCriteria(TorComplains.class, "cps")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());

		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorComplains> findByExample(TorComplains example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorComplains.class, "cps")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

}
