package com.tockacrta.trailOer.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.tockacrta.trailOer.dao.TorCmrCmnDAO;
import com.tockacrta.trailOer.model.TorCmrCmn;
import com.tockacrta.trailOer.model.TorCompetitor;
import com.tockacrta.trailOer.model.TorEvent;


public class TorCmrCmnDAOImpl  extends BaseHibernateDAOImpl<TorCmrCmn, Long> implements TorCmrCmnDAO {

	@Override
	void setClazz() {
		super.clazz = TorCmrCmn.class;
	}

	@Override
	public int findByExampleSize(TorCmrCmn example) {
		Criteria criteria = getCurrentSession().createCriteria(TorCmrCmn.class, "cmm")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());

		criteria =criteria.setProjection(Projections.rowCount());
		return  (Integer) ((Long)criteria.uniqueResult()).intValue();
	}

	@Override
	public List<TorCmrCmn> findByExample(TorCmrCmn example, int firstElement,
			int maxElements, Map<String, Boolean> sorts) {
		Criteria criteria = getCurrentSession().createCriteria(TorCmrCmn.class, "cmm")
				.add(Example.create(example)
						.enableLike(MatchMode.START)
						.ignoreCase()
						.excludeZeroes());
	
		criteria=addSort(criteria, sorts);
		criteria.setFirstResult(firstElement);
		if (maxElements > 0) {
			criteria.setMaxResults(maxElements);
		}
		return criteria.list();
	}

	@Override
	public List<TorCmrCmn> findByCmrAndEvn(TorCompetitor cmr, TorEvent evn) {
		Criteria criteria = getCurrentSession().createCriteria(TorCmrCmn.class, "cmm")
					.add(Restrictions.eq("cmm.torCompetitor", cmr));
		criteria=criteria.createCriteria("cmm.torCompetition", "cmn")
					.add(Restrictions.eq("cmn.torEvent", evn));
		List<TorCmrCmn> result=criteria.list();
		return result;
	}


}
