package com.tockacrta.trailOer.model;
// Generated May 26, 2014 12:45:38 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * TohEvent generated by hbm2java
 */
public class TohEvent implements java.io.Serializable {

	private Long id;
	private Long idm;
	private String cudm;
	private String code;
	private String name;
	private Date startDate;
	private Date endDate;
	private String createdBy;
	private Date dateCreated;
	private Set tohCompetitions = new HashSet(0);
	private TohTypes torTypeStatus;
	private String nameInt;

	public TohEvent() {
	}
	
	public TohEvent(TorEvent torEvent, String cudm) {
		
		this.cudm=cudm;
		this.idm = torEvent.getId();
		this.code = torEvent.getCode();
		this.name = torEvent.getName();
		this.startDate = torEvent.getStartDate();
		this.endDate = torEvent.getEndDate();
		this.createdBy = torEvent.getCreatedBy();
		this.dateCreated = torEvent.getDateCreated();
	}

	public TohEvent(Long id, Long idm, String cudm, String code, String name) {
		this.id = id;
		this.idm = idm;
		this.cudm = cudm;
		this.code = code;
		this.name = name;
	}

	public TohEvent(Long id, Long idm, String cudm, String code, String name,
			Date startDate, Date endDate, String createdBy, Date dateCreated,
			Set tohCompetitions) {
		this.id = id;
		this.idm = idm;
		this.cudm = cudm;
		this.code = code;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.tohCompetitions = tohCompetitions;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getIdm() {
		return this.idm;
	}

	public void setIdm(Long idm) {
		this.idm = idm;
	}

	public String getCudm() {
		return this.cudm;
	}

	public void setCudm(String cudm) {
		this.cudm = cudm;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Set getTohCompetitions() {
		return this.tohCompetitions;
	}

	public void setTohCompetitions(Set tohCompetitions) {
		this.tohCompetitions = tohCompetitions;
	}

	public TohTypes getTorTypeStatus() {
		return torTypeStatus;
	}

	public void setTorTypeStatus(TohTypes torTypeStatus) {
		this.torTypeStatus = torTypeStatus;
	}

	public String getNameInt() {
		return nameInt;
	}

	public void setNameInt(String nameInt) {
		this.nameInt = nameInt;
	}

}
