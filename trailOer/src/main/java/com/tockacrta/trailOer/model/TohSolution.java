package com.tockacrta.trailOer.model;
// Generated May 26, 2014 12:45:38 PM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;
import java.util.Date;

/**
 * TohSolution generated by hbm2java
 */
public class TohSolution implements java.io.Serializable {

	private Long id;
	private TohTask tohTask;
	private TohCmrCmnRun tohCmrCmnRun;
	private Long idm;
	private String cudm;
	private String solution;
	private BigDecimal time;
	private String createdBy;
	private Date dateCreated;
	private TohTypes tohTypeStatus;
	private String notes;

	public TohSolution() {
	}

	public TohSolution(Long id, TohTask tohTask, TohCmrCmnRun tohCmrCmnRun,
			Long idm, String cudm, String solution, BigDecimal time) {
		this.id = id;
		this.tohTask = tohTask;
		this.tohCmrCmnRun = tohCmrCmnRun;
		this.idm = idm;
		this.cudm = cudm;
		this.solution = solution;
		this.time = time;
	}

	public TohSolution(Long id, TohTask tohTask, TohCmrCmnRun tohCmrCmnRun,
			Long idm, String cudm, String solution, BigDecimal time, String createdBy,
			Date dateCreated) {
		this.id = id;
		this.tohTask = tohTask;
		this.tohCmrCmnRun = tohCmrCmnRun;
		this.idm = idm;
		this.cudm = cudm;
		this.solution = solution;
		this.time = time;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TohTask getTohTask() {
		return this.tohTask;
	}

	public void setTohTask(TohTask tohTask) {
		this.tohTask = tohTask;
	}

	public TohCmrCmnRun getTohCmrCmnRun() {
		return this.tohCmrCmnRun;
	}

	public void setTohCmrCmnRun(TohCmrCmnRun tohCmrCmnRun) {
		this.tohCmrCmnRun = tohCmrCmnRun;
	}

	public long getIdm() {
		return this.idm;
	}

	public void setIdm(Long idm) {
		this.idm = idm;
	}

	public String getCudm() {
		return this.cudm;
	}

	public void setCudm(String cudm) {
		this.cudm = cudm;
	}

	public String getSolution() {
		return this.solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public BigDecimal getTime() {
		return this.time;
	}

	public void setTime(BigDecimal time) {
		this.time = time;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public TohTypes getTohTypeStatus() {
		return tohTypeStatus;
	}

	public void setTohTypeStatus(TohTypes tohTypeStatus) {
		this.tohTypeStatus = tohTypeStatus;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}


}
