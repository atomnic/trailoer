package com.tockacrta.trailOer.model;
// Generated May 26, 2014 12:45:38 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * TorRun generated by hbm2java
 */
public class TorRun implements java.io.Serializable {

	private Long id;
	private TorTypes torTypes;
	private Date startTime;
	private String createdBy;
	private Date dateCreated;
	private Set torCmrCmnRuns = new HashSet(0);
	private TorCompetition torCompetition;
	private String code;
	private String name;
	private String description;
	private TorTypes torTypeStatus;
	private Set torTasks = new HashSet(0);
	private String nameInt;
	private String startTimeGroup;
	private Integer timeLimit;
	
	public TorRun() {
	}

	public TorRun(Long id, TorTypes torTypes, Date startTime) {
		this.id = id;
		this.torTypes = torTypes;
		this.startTime = startTime;
	}

	public TorRun(Long id, TorTypes torTypes, Date startTime, String createdBy,
			Date dateCreated, Set torCmrCmnRuns) {
		this.id = id;
		this.torTypes = torTypes;
		this.startTime = startTime;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.torCmrCmnRuns = torCmrCmnRuns;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TorTypes getTorTypes() {
		return this.torTypes;
	}

	public void setTorTypes(TorTypes torTypes) {
		this.torTypes = torTypes;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Set getTorCmrCmnRuns() {
		return this.torCmrCmnRuns;
	}

	public void setTorCmrCmnRuns(Set torCmrCmnRuns) {
		this.torCmrCmnRuns = torCmrCmnRuns;
	}

	public TorCompetition getTorCompetition() {
		return torCompetition;
	}

	public void setTorCompetition(TorCompetition torCompetition) {
		this.torCompetition = torCompetition;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public TorTypes getTorTypeStatus() {
		return torTypeStatus;
	}

	public void setTorTypeStatus(TorTypes torTypeStatus) {
		this.torTypeStatus = torTypeStatus;
	}

	public Set getTorTasks() {
		return torTasks;
	}

	public void setTorTasks(Set torTasks) {
		this.torTasks = torTasks;
	}

	public String getNameInt() {
		return nameInt;
	}

	public String getStartTimeGroup() {
		return startTimeGroup;
	}

	public void setStartTimeGroup(String startTimeGroup) {
		this.startTimeGroup = startTimeGroup;
	}

	public void setNameInt(String nameInt) {
		this.nameInt = nameInt;
	}

	public Integer getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(Integer timeLimit) {
		this.timeLimit = timeLimit;
	}	

}
