package com.tockacrta.trailOer.model;
// Generated May 26, 2014 12:45:38 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * TohTask generated by hbm2java
 */
public class TohTask implements java.io.Serializable {

	private Long id;
	private TohTypes tohTypeStatus;
	private TohTypes tohTypeType;
	private Long idm;
	private String cudm;
	private Integer no;
	private String solution;
	private String createdBy;
	private Date dateCreated;
	private Set tohSolutions = new HashSet(0);
	private Set tohComplainses = new HashSet(0);
	private TohRun tohRun;
	private TohTask tohTask;
	private Set tohTasks = new HashSet(0);
	private String fullNo;
	private TohCategory tohCategory;		

	public TohTask() {
	}

	public TohTask(Long id, TohTypes tohTypesByTypIdType, Long idm,
			String cudm, int no) {
		this.id = id;
		this.tohTypeType = tohTypesByTypIdType;
		this.idm = idm;
		this.cudm = cudm;
		this.no = no;
	}

	public TohTask(Long id, TohTypes tohTypesByTypIdStatus,
			TohTypes tohTypesByTypIdType, Long idm, String cudm, int no,
			String solution, String createdBy, Date dateCreated,
			Set tohSolutions, Set tohComplainses) {
		this.id = id;
		this.tohTypeStatus = tohTypesByTypIdStatus;
		this.tohTypeType = tohTypesByTypIdType;
		this.idm = idm;
		this.cudm = cudm;
		this.no = no;
		this.solution = solution;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.tohSolutions = tohSolutions;
		this.tohComplainses = tohComplainses;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TohTypes getTohTypeStatus() {
		return this.tohTypeStatus;
	}

	public void setTohTypeStatus(TohTypes tohTypeStatus) {
		this.tohTypeStatus = tohTypeStatus;
	}

	public TohTypes getTohTypeType() {
		return this.tohTypeType;
	}

	public void setTohTypeType(TohTypes tohTypeType) {
		this.tohTypeType = tohTypeType;
	}

	public long getIdm() {
		return this.idm;
	}

	public void setIdm(Long idm) {
		this.idm = idm;
	}

	public String getCudm() {
		return this.cudm;
	}

	public void setCudm(String cudm) {
		this.cudm = cudm;
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getSolution() {
		return this.solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Set getTohSolutions() {
		return this.tohSolutions;
	}

	public void setTohSolutions(Set tohSolutions) {
		this.tohSolutions = tohSolutions;
	}

	public Set getTohComplainses() {
		return this.tohComplainses;
	}

	public void setTohComplainses(Set tohComplainses) {
		this.tohComplainses = tohComplainses;
	}

	public TohRun getTohRun() {
		return tohRun;
	}

	public void setTohRun(TohRun tohRun) {
		this.tohRun = tohRun;
	}

	public Set getTohTasks() {
		return tohTasks;
	}

	public void setTohTasks(Set tohTasks) {
		this.tohTasks = tohTasks;
	}

	public TohTask getTohTask() {
		return tohTask;
	}

	public void setTohTask(TohTask tohTask) {
		this.tohTask = tohTask;
	}


	public String getFullNo() {
		return fullNo;
	}

	public void setFullNo(String fullNo) {
		this.fullNo = fullNo;
	}
	
	public TohCategory getTohCategory() {
		return tohCategory;
	}

	public void setTohCategory(TohCategory tohCategory) {
		this.tohCategory = tohCategory;
	}	
	
}
