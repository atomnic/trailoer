package com.tockacrta.trailOer.model;
// Generated May 26, 2014 12:45:38 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * TohClub generated by hbm2java
 */
public class TohClub implements java.io.Serializable {

	private Long id;
	private TohCountry tohCountry;
	private Long idm;
	private String cudm;
	private String code;
	private String name;
	private String createdBy;
	private Date dateCreated;
	private Set tohCompetitors = new HashSet(0);

	public TohClub() {
	}

	public TohClub(Long id, Long idm, String cudm, String code, String name) {
		this.id = id;
		this.idm = idm;
		this.cudm = cudm;
		this.code = code;
		this.name = name;
	}

	public TohClub(Long id, TohCountry tohCountry, Long idm, String cudm,
			String code, String name, String createdBy, Date dateCreated,
			Set tohCompetitors) {
		this.id = id;
		this.tohCountry = tohCountry;
		this.idm = idm;
		this.cudm = cudm;
		this.code = code;
		this.name = name;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.tohCompetitors = tohCompetitors;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TohCountry getTohCountry() {
		return this.tohCountry;
	}

	public void setTohCountry(TohCountry tohCountry) {
		this.tohCountry = tohCountry;
	}

	public long getIdm() {
		return this.idm;
	}

	public void setIdm(Long idm) {
		this.idm = idm;
	}

	public String getCudm() {
		return this.cudm;
	}

	public void setCudm(String cudm) {
		this.cudm = cudm;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Set getTohCompetitors() {
		return this.tohCompetitors;
	}

	public void setTohCompetitors(Set tohCompetitors) {
		this.tohCompetitors = tohCompetitors;
	}

}
